import numpy as np
import dolfin as df
import os
from os.path import join
import pylab as plt
import ConductivityClass
from plotting_convention import *
from params import *


def extract_crossection_plane(phi, sim_name):
    values = np.zeros(crossection_x.shape)
    for col in xrange(crossection_x.shape[1]):
        for row in xrange(crossection_x.shape[0]):
            values[row, col] = phi(crossection_x[row, col], 10.0, crossection_z[row, col])
    np.save('%s_crossection.npy' % sim_name, values)
    return values


def extract_MEA_plane(phi, sim_name, elec_z_pos):
    values = np.zeros(plane_x.shape)
    for col in xrange(plane_x.shape[1]):
        for row in xrange(plane_x.shape[0]):
            values[row, col] = phi(plane_x[row, col], plane_y[row, col], elec_z_pos)
    np.save('%s_mea_plane.npy' % sim_name, values)
    return values


def make_figure_6():

    out_folder = 'point_source_results'
    plt.close('all')
    fig = plt.figure(figsize=[5.4, 6])
    zpos = 'z2'
    fem_mea_plane = {}
    fem_crossection = {}
    moi_mea_plane = {}
    moi_crossection = {}

    max_plane = []
    max_crossection = []
    param_sets = [set_control, set_d, set_anis2, set_k, set_sl5]
    for param_set in param_sets:
        name = param_set['set_name']
        fem_name = join(out_folder, 'fem_%s_%s' % (name, zpos))
        moi_name = join(out_folder, 'moi_%s_%s' % (name, zpos))
        if os.path.isfile('%s_mea_plane.npy' %fem_name):
            fem_mea_plane[name] = np.load('%s_mea_plane.npy' % fem_name)
            max_plane.append(np.max(fem_mea_plane[name]))
        if os.path.isfile('%s_mea_plane.npy' %moi_name):
            moi_mea_plane[name] = np.load('%s_mea_plane.npy' % moi_name)
            max_plane.append(np.max(moi_mea_plane[name]))
        if os.path.isfile('%s_crossection.npy' %fem_name):
            fem_crossection[name] = np.load('%s_crossection.npy' % fem_name)
            max_crossection.append(np.max(fem_crossection[name]))
        if os.path.isfile('%s_crossection.npy' %moi_name):
            moi_crossection[name] = np.load('%s_crossection.npy' % moi_name)
            max_crossection.append(np.max(moi_crossection[name]))

    max_plane = np.max(max_plane)
    max_crossection = np.max(max_crossection)
    n_plot_rows = 5
    clevels_crossection = np.array([0.5**d for d in np.arange(8, -1, -1)])
    clevels_mea_plane = np.array([0.75**d for d in np.arange(8, -1, -1)])

    cmap = plt.cm.gray_r
    ax_width = 0.18
    ax_height = 0.18
    field_extent = 450
    ax1_start = 0.25
    ax2_start = ax1_start + 1.2*ax_width*1.5
    ax3_start = ax2_start + 1.2*ax_width

    ax_markers = list('abcdefghijklmnopqrst')
    for row, param_set in enumerate(param_sets):

        name = param_set['set_name']

        ax_height_start = .94*(n_plot_rows - row - 1)/n_plot_rows + 0.02
        ax1 = fig.add_axes([ax1_start, ax_height_start, ax_width*1.5, ax_height],
                           xlim=[-field_extent, field_extent], ylim=[-150, 350],
                           frameon=False, xticks=[], yticks=[], aspect='equal')
        ax1.text(-1240, 0, param_set['description'], size=7)

        CS = fem_crossection[name]/max_crossection

        img1 = ax1.contourf(crossection_x, crossection_z, CS,
                            levels=clevels_crossection, cmap=cmap, norm=LogNorm(), rasterized=True)
        ax1.contour(crossection_x, crossection_z, CS, levels=clevels_crossection, norm=LogNorm(),
                    colors='k', rasterized=True)

        mark_subplots(ax1, ax_markers.pop(0))

        if name in fem_mea_plane:
            fem_mea = fem_mea_plane[name]/max_plane

            ax2 = fig.add_axes([ax2_start, ax_height_start, ax_width, ax_height],
                               xlim=[-field_extent, field_extent], aspect='equal',
                               ylim=[-field_extent, field_extent],
                               frameon=False, xticks=[], yticks=[])
            img2 = ax2.contourf(plane_x, plane_y, fem_mea, levels=clevels_mea_plane,
                                cmap=cmap, norm=LogNorm(), rasterized=True)
            ax2.contour(plane_x, plane_y, fem_mea, levels=clevels_mea_plane,
                        norm=LogNorm(), colors='k', rasterized=True)
            mark_subplots(ax2, ax_markers.pop(0))

        if name in moi_mea_plane:
            moi_mea = moi_mea_plane[name] / max_plane
            ax3 = fig.add_axes([ax3_start, ax_height_start, ax_width, ax_height],
                               xlim=[-field_extent, field_extent], aspect='equal',
                               ylim=[-field_extent, field_extent],
                               frameon=False, xticks=[], yticks=[])
            img3 = ax3.contourf(plane_x, plane_y, moi_mea, levels=clevels_mea_plane,
                                cmap=cmap, norm=LogNorm(), rasterized=True)
            ax3.contour(plane_x, plane_y, moi_mea, levels=clevels_mea_plane,
                        norm=LogNorm(), colors='k', rasterized=True)
            mark_subplots(ax3, ax_markers.pop(0))

        if not param_set['set_name'] in ['a', 'i']:
            ax1.plot([-500, 500], [+elec_z, +elec_z], lw=1.5, color='k')
            ax1.plot([-500, 500], [-elec_z, -elec_z], lw=1.5, color='k')

        if param_set['set_name'] is 'control':
            ax1.set_xlabel(r'x $\rightarrow$', size=12)
            ax1.set_ylabel(r'z $\rightarrow$', size=12)
            ax2.set_xlabel(r'x $\rightarrow$', size=12, va='center')
            ax2.set_ylabel(r'y $\rightarrow$', size=12)

        if param_set['set_name'] == 'control':
            ax1.set_title('Side view')
            ax2.set_title('MEA plane\nFEM')
            ax3.set_title('MEA plane\nMoI')

        if 'inhomo_x_pos' in param_set:
            ax1.plot([param_set['inhomo_x_pos'], param_set['inhomo_x_pos']],
                     [elec_z, -elec_z], '--', lw=1, color='k')
            ax2.plot([param_set['inhomo_x_pos'], param_set['inhomo_x_pos']],
                     [-field_extent, field_extent], '--', lw=1, color='k')
            ax1.text(-420, 70, 'T1', color='k', size=8)
            ax1.text(350, 70, 'T2', color='k', size=8)
            ax2.text(-400, -440, 'T1', color='k', size=8)
            ax2.text(250, -440, 'T2', color='k', size=8)
        if 'interface_thickness' in param_set:
            ax1.plot([-field_extent, field_extent],
                     [elec_z + param_set['interface_thickness'], elec_z + param_set['interface_thickness']],
                     '-.', lw=1, color='k')
    fig.savefig('figure_6.png', dpi=150)
    # fig.savefig('PS_fields_new.pdf', dpi=150)

def plot_saline_interface_cross_section():

    out_folder = 'point_source_results'
    plt.close('all')
    fig = plt.figure(figsize=[7, 6])
    fig.subplots_adjust(top=0.98, bottom=0.02, hspace=0.2, wspace=0.2, left=0.01, right=0.99)
    zpos = 'zi'
    fem_mea_plane = {}
    fem_crossection = {}
    max_plane = []
    max_crossection = []
    param_sets = [set_control, set_sl5]
    for param_set in param_sets:
        name = param_set['set_name']
        print name
        fem_name = join(out_folder, 'fem_%s_%s' % (name, zpos))
        if os.path.isfile('%s_mea_plane.npy' % fem_name):
            fem_mea_plane[name] = np.load('%s_mea_plane.npy' % fem_name)
            max_plane.append(np.max(fem_mea_plane[name]))
        if os.path.isfile('%s_crossection.npy' %  fem_name):
            fem_crossection[name] = np.load('%s_crossection.npy' % fem_name)
            max_crossection.append(np.max(fem_crossection[name]))

    max_plane = np.max(max_plane)
    max_crossection = np.max(max_crossection)
    clevels_crossection = np.array([0.5**d for d in np.arange(16, -1, -1)])
    clevels_mea_plane = np.array([0.75**d for d in np.arange(16, -1, -1)])

    cmap = plt.cm.jet
    field_extent = 450
    name = param_sets[0]['set_name']
    ax1 = fig.add_subplot(3, 2, 1,
                       xlim=[-field_extent, field_extent], ylim=[-150, 350],
                       frameon=False, xticks=[], yticks=[], aspect='equal')

    CS = fem_crossection[name]/max_crossection

    img1 = ax1.pcolormesh(crossection_x, crossection_z, CS, vmax=1,
                        cmap=cmap, norm=LogNorm(), rasterized=True)
    ax1.contour(crossection_x, crossection_z, CS, levels=clevels_crossection, norm=LogNorm(),
                colors='k', rasterized=True)
    plt.colorbar(img1)
    fem_mea = fem_mea_plane[name]/max_plane

    ax2 = fig.add_subplot(3, 2, 2,
                       xlim=[-field_extent, field_extent], aspect='equal',
                       ylim=[-field_extent, field_extent],
                       frameon=False, xticks=[], yticks=[])
    img2 = ax2.pcolormesh(plane_x, plane_y, fem_mea, norm=LogNorm(), vmax=1,
                        cmap=cmap,  rasterized=True)
    plt.colorbar(img2)

    ax2.contour(plane_x, plane_y, fem_mea, levels=clevels_mea_plane,
                norm=LogNorm(), colors='k', rasterized=True)

    ax1.set_title(name)

    name = param_sets[1]['set_name']

    ax1 = fig.add_subplot(3, 2, 3,
                       xlim=[-field_extent, field_extent], ylim=[-150, 350],
                       frameon=False, xticks=[], yticks=[], aspect='equal')

    CS = fem_crossection[name]/max_crossection

    img1 = ax1.pcolormesh(crossection_x, crossection_z, CS,
                        cmap=cmap, norm=LogNorm(), rasterized=True, vmax=1)
    ax1.contour(crossection_x, crossection_z, CS, levels=clevels_crossection, norm=LogNorm(),
                colors='k', rasterized=True)
    plt.colorbar(img1)

    fem_mea = fem_mea_plane[name]/max_plane

    ax2 = fig.add_subplot(3, 2, 4,
                       xlim=[-field_extent, field_extent], aspect='equal',
                       ylim=[-field_extent, field_extent],
                       frameon=False, xticks=[], yticks=[])
    img2 = ax2.pcolormesh(plane_x, plane_y, fem_mea, norm=LogNorm(),
                        cmap=cmap,  rasterized=True, vmax=1)
    plt.colorbar(img2)

    ax2.contour(plane_x, plane_y, fem_mea, levels=clevels_mea_plane,
                   norm=LogNorm(), colors='k', rasterized=True)
    ax1.set_title("Saline interface (30 $\mu m$)")
    ax1.plot([-field_extent, field_extent],
                 [elec_z + param_sets[1]['interface_thickness'], elec_z + param_sets[1]['interface_thickness']],
                 '-.', lw=1, color='k')

    diff_fem_mea = np.abs(fem_mea_plane[param_sets[1]['set_name']] - fem_mea_plane[param_sets[0]['set_name']])/max_plane
    diff_fem_CS = np.abs(fem_crossection[param_sets[1]['set_name']] - fem_crossection[param_sets[0]['set_name']])/max_crossection
    ax1 = fig.add_subplot(3, 2, 5,
                       xlim=[-field_extent, field_extent], ylim=[-150, 350],
                       frameon=False, xticks=[], yticks=[], aspect='equal')

    ax2 = fig.add_subplot(3, 2, 6,
                       xlim=[-field_extent, field_extent], aspect='equal',
                       ylim=[-field_extent, field_extent],
                       frameon=False, xticks=[], yticks=[])
    img1 = ax1.pcolormesh(crossection_x, crossection_z, diff_fem_CS, cmap=cmap,  rasterized=True, norm=LogNorm())
    plt.colorbar(img1, ax=ax1)

    img2 = ax2.pcolormesh(plane_x, plane_y, diff_fem_mea, cmap=cmap,  rasterized=True, norm=LogNorm())
    plt.colorbar(img2, ax=ax2)

    fig.savefig('saline_interface_test.png', dpi=150)
    # fig.savefig('PS_fields_new.pdf', dpi=150)

def make_figure_8():

    out_folder = 'point_source_results'
    plt.close('all')
    fig = plt.figure(figsize=[3.2, 2.])
    fig.subplots_adjust(bottom=0.4, top=0.86)
    rms_arrays = lambda array1, array2: np.sqrt(np.average((array1 - array2)**2))

    param_sets = [set_d, set_anis2]
    dist = []
    for zpos_name, z in charge_z_positions.items():
        dist.append(z + 150.)
    argsort_dist = np.argsort(dist)
    dist = np.array(dist)[argsort_dist]
    ax1 = fig.add_subplot(121, xlim=[-5, 300], xticks=[0, 100, 200, 300],
                          ylim=[5e-4, 1e-2], ylabel='RMS', xlabel='Source height [$\mu m$]')
    ax2 = fig.add_subplot(122, xlim=[-5, 300], xticks=[0, 100, 200, 300],
                          ylim=[1e-3, 1e-0], xlabel='Source height [$\mu m$]', ylabel='Relative RMS error')
    lines = []
    line_names = []

    rms_lines = []
    rms_line_names = []

    l2, = ax1.semilogy(0, 0, '-', c='gray', lw=1.0)
    l3, = ax1.semilogy(0, 0, '--', c='gray', lw=1.0)

    rms_lines.extend([l2, l3])
    rms_line_names.extend(['RMS FEM', 'RMS MoI'])

    normalize_const = np.max(np.load(join(out_folder, 'fem_d_za_mea_plane.npy')))
    for param_set in param_sets:
        name = param_set['set_name']
        fem_rms_list = []
        rms_diff_list = []
        moi_rms_list = []
        for zpos_name, z in charge_z_positions.items():
            fem_name = join(out_folder, 'fem_%s_%s' % (name, zpos_name))
            moi_name = join(out_folder, 'moi_%s_%s' % (name, zpos_name))
            fem = np.load('%s_mea_plane.npy' % fem_name) / normalize_const
            moi = np.load('%s_mea_plane.npy' % moi_name) / normalize_const
            fem_rms_list.append(np.sqrt(np.average(fem**2)))
            moi_rms_list.append(np.sqrt(np.average(moi**2)))
            rms_diff_list.append(rms_arrays(fem, moi))
        moi_rms = np.array(moi_rms_list)[argsort_dist]
        fem_rms = np.array(fem_rms_list)[argsort_dist]
        rms_diff = np.array(rms_diff_list)[argsort_dist]
        relative_rms_diff = rms_diff / fem_rms

        # ax1.semilogy(dist, rms_diff, c=param_set['clr'], lw=1.5)
        ax1.semilogy(dist, fem_rms, '-', c=param_set['clr'], lw=1)
        ax1.semilogy(dist, moi_rms, '--', c=param_set['clr'], lw=1., alpha=1)
        lines.append(ax1.semilogy(0, 0, c=param_set['clr'], lw=1)[0])
        line_names.append(param_set['description'])
        ax2.semilogy(dist, relative_rms_diff, c=param_set['clr'], lw=1.5)

    mark_subplots([ax1, ax2])
    simplify_axes([ax1, ax2])
    ax1.grid(True)
    ax2.grid(True)
    ax1.legend(rms_lines, rms_line_names, frameon=False, handlelength=3, bbox_to_anchor=[1.15, 1.35])
    fig.legend(lines, line_names, frameon=False, ncol=2, loc='lower center')
    fig.savefig('figure_8.png', dpi=150)


def make_figure_4():

    out_folder = 'point_source_results'

    zpos = 'z2'
    fem_mea_plane = {}
    fem_crossection = {}
    moi_mea_plane = {}
    moi_mea_plane_decay = {}
    fem_mea_plane_decay = {}
    moi_rms_mea_plane_decay = {}
    fem_rms_mea_plane_decay = {}
    max_plane_moi = []
    max_plane_fem = []
    max_crossection = []
    param_sets = [set_control, set_a]
    rms_arrays = lambda array1, array2: np.sqrt(np.average((array1 - array2)**2))

    rms_mea_plane = {}
    dist = []
    for zpos_name, z in charge_z_positions.items():
        dist.append(z + 150.)
    argsort_dist = np.argsort(dist)
    dist = np.array(dist)[argsort_dist]

    for param_set in param_sets:
        name = param_set['set_name']
        fem_name = join(out_folder, 'fem_%s_%s' % (name, zpos))
        moi_name = join(out_folder, 'moi_%s_%s' % (name, zpos))
        fem_mea_plane[name] = np.load('%s_mea_plane.npy' % fem_name)
        moi_mea_plane[name] = np.load('%s_mea_plane.npy' % moi_name)
        fem_crossection[name] = np.load('%s_crossection.npy' % fem_name)
        max_plane_fem.append(np.max(fem_mea_plane[name]))
        max_plane_moi.append(np.max(moi_mea_plane[name]))
        max_crossection.append(np.max(fem_crossection[name]))
        fem_list = []
        moi_list = []
        fem_rms_list = []
        moi_rms_list = []
        rms_list = []
        for zpos_name, z in charge_z_positions.items():
            fem_name = join(out_folder, 'fem_%s_%s' % (name, zpos_name))
            moi_name = join(out_folder, 'moi_%s_%s' % (name, zpos_name))
            fem = np.load('%s_mea_plane.npy' % fem_name)
            moi = np.load('%s_mea_plane.npy' % moi_name)
            fem_list.append(np.max(fem))
            moi_list.append(np.max(moi))
            fem_rms_list.append(np.sqrt(np.average(fem**2)))
            moi_rms_list.append(np.sqrt(np.average(moi**2)))
            rms_list.append(rms_arrays(fem, moi))
        rms_mea_plane[name] = np.array(rms_list)[argsort_dist]
        moi_mea_plane_decay[name] = np.array(moi_list)[argsort_dist]
        fem_mea_plane_decay[name] = np.array(fem_list)[argsort_dist]
        moi_rms_mea_plane_decay[name] = np.array(moi_rms_list)[argsort_dist]
        fem_rms_mea_plane_decay[name] = np.array(fem_rms_list)[argsort_dist]

    max_plane = np.max([max_plane_fem, max_plane_moi])
    max_crossection = np.max(max_crossection)

    clevels_crossection = np.array([0.5**d for d in np.arange(8, -1, -1)])
    clevels_mea_plane = np.array([0.75**d for d in np.arange(8, -1, -1)])

    cmap = plt.cm.gray_r
    field_extent = 450
    plt.close('all')
    fig = plt.figure(figsize=[6.4, 2.8])
    fig.subplots_adjust(left=0.15, top=0.85, hspace=0.5, wspace=0.0, right=0.96)

    ylabel = 'Center potential'
    row_names = ['300 $\mu m$ thick slice', 'Semi-infinite slice']

    for row, param_set in enumerate(param_sets):
        name = param_set['set_name']
        fem_mea = fem_mea_plane[name] / max_plane
        moi_mea = moi_mea_plane[name] / max_plane
        ax1 = fig.add_axes([0.05, 0.6 - row*0.45, 0.17, 0.27], frameon=False, xticks=[], yticks=[],
                           xlim=[-field_extent, field_extent], ylim=[-150, 350],
                           aspect='equal')
        ax2 = fig.add_axes([0.19, 0.6 - row*0.45, 0.24, 0.28], frameon=False, xticks=[], yticks=[],
                           xlim=[-field_extent, field_extent], aspect='equal',
                           ylim=[-field_extent, field_extent])
        ax3 = fig.add_axes([0.32, 0.6 - row*0.45, 0.24, 0.28], frameon=False, xticks=[], yticks=[],
                           xlim=[-field_extent, field_extent], aspect='equal',
                           ylim=[-field_extent, field_extent])
        ax4 = fig.add_axes([0.57, 0.62 - row*0.44, 0.1, 0.25], xlim=[-5, 300], xticks=[0, 100, 200, 300],
                           ylim=[1e-5, 1e0], ylabel=ylabel)
        ax5 = fig.add_axes([0.76, 0.62 - row*0.44, 0.1, 0.25], xlim=[-5, 300], xticks=[0, 100, 200, 300],
                           ylim=[1e-4, 1e-1], xlabel='Source height [$\mu m$]', ylabel='Relative error')
        ax6 = fig.add_axes([0.89, 0.62 - row*0.44, 0.1, 0.25], xlim=[0, 30], xticks=[0, 10, 20, 30], yticklabels=[],
                           ylim=[1e-4, 1e-1])
        ax4.grid(True)
        ax5.grid(True)
        ax6.grid(True)
        simplify_axes([ax4, ax5, ax6])
        ax1.text(-700, 100, row_names[row], size=8, rotation='vertical', va='center')

        CS = fem_crossection[name]/max_crossection

        img1 = ax1.contourf(crossection_x, crossection_z, CS, norm=LogNorm(),
                            levels=clevels_crossection, cmap=cmap, rasterized=True)

        ax1.contour(crossection_x, crossection_z, CS, levels=clevels_crossection,
                    colors='k', rasterized=True)
        img2 = ax2.contourf(plane_x, plane_y, fem_mea, levels=clevels_mea_plane,
                            cmap=cmap, norm=LogNorm(), rasterized=True)
        ax2.contour(plane_x, plane_y, fem_mea, levels=clevels_mea_plane,
                    norm=LogNorm(), colors='k', rasterized=True)

        img3 = ax3.contourf(plane_x, plane_y, moi_mea, levels=clevels_mea_plane,
                            cmap=cmap, norm=LogNorm(), rasterized=True)
        ax3.contour(plane_x, plane_y, moi_mea, levels=clevels_mea_plane,
                    norm=LogNorm(), colors='k', rasterized=True)


        l1, = ax4.semilogy(dist, fem_mea_plane_decay[name], color='k', lw=2)
        l2, = ax4.semilogy(dist, moi_mea_plane_decay[name], '--', color='r', lw=2)
        l3, = ax4.semilogy(dist, np.abs(moi_mea_plane_decay[name] - fem_mea_plane_decay[name]), c='b', lw=2)
        l4, = ax5.semilogy(dist, np.abs(moi_mea_plane_decay[name] - fem_mea_plane_decay[name]) /
                           moi_mea_plane_decay[name], c='g', lw=2)
        l5, = ax6.semilogy(dist, np.abs(moi_mea_plane_decay[name] - fem_mea_plane_decay[name]) /
                           moi_mea_plane_decay[name], c='g', lw=2)

        ax4.set_yticks(ax4.get_yticks()[1:-1:2])
        ax6.set_yticklabels([])
        if not param_set['set_name'] in ['a', 'i']:
            ax1.plot([-500, 500], [+elec_z, +elec_z], lw=1.5, color='k')
            ax1.plot([-500, 500], [-elec_z, -elec_z], lw=1.5, color='k')

        if param_set['set_name'] == 'control':
            ax1.set_title('Side view')
            ax2.set_title('MEA plane\nFEM')
            ax3.set_title('MEA plane\nMoI')
            ax1.text(-50, -220, r'x $\rightarrow$', size=8)
            ax1.text(-520, 100, r'z $\rightarrow$', rotation='vertical', size=8)

            ax2.text(-50, -550, r'x $\rightarrow$', size=8)
            ax2.text(-570, 100, r'y $\rightarrow$', rotation='vertical', size=8)

    fig.legend([l1, l2, l3, l4], ['FEM', 'MoI', 'Difference', 'Relative difference'],
               frameon=False, loc='lower right', ncol=5, handlelength=3)
    mark_subplots(fig.axes, xpos=0.05, ypos=1.2)
    fig.savefig('figure_4.png', dpi=150)


def fem_simulation(param_set, zpos_name, mesh, boundaries, subdomains, V, sigma, out_folder):
    """ Here the Finite Element calculations are done"""

    v = df.TestFunction(V)
    u = df.TrialFunction(V)
    a = df.inner(sigma * df.grad(u), df.grad(v))*df.dx(1)

    L = df.Constant(0)*v*df.dx
    bc = df.DirichletBC(V, df.Constant(0), boundaries, 9)
    phi = df.Function(V)
    A = df.assemble(a, cell_domains=subdomains, exterior_facet_domains=boundaries)
    b = df.assemble(L, cell_domains=subdomains, exterior_facet_domains=boundaries)
    bc.apply(A, b)
    point = df.Point(0, 0, charge_z_positions[zpos_name])
    delta = df.PointSource(V, point)
    delta.apply(b)
    solver = df.KrylovSolver("cg", "ilu")
    solver.solve(A, phi.vector(), b)

    sim_name = join(out_folder, 'fem_%s_%s' % (param_set['set_name'], zpos_name))
    fem_crossection = extract_crossection_plane(phi, sim_name)
    fem_mea_plane = extract_MEA_plane(phi, sim_name, -slice_thickness/2. + 1e-8)


def make_figure_7():
    """ This function makes the main summary plot of the different conditions for the
    Point Source (PS) case """

    param_sets = [set_a, set_d, set_anis2, set_f, set_k]
    out_folder = 'point_source_results'
    rms_arrays = lambda array1, array2: np.sqrt(np.average((array1 - array2)**2))

    errors = {}
    rel_max_diff = {}
    rms = {}
    control_rms = np.zeros(len(charge_z_positions))
    charge_heights = np.zeros(len(charge_z_positions))

    normalize_constant = np.sqrt(np.average(np.load(join(out_folder, 'fem_control_za_mea_plane.npy'))**2))
    for param_set in param_sets:
        errors[param_set['set_name']] = np.zeros(len(charge_z_positions))
        rel_max_diff[param_set['set_name']] = np.zeros(len(charge_z_positions))
        rms[param_set['set_name']] = np.zeros(len(charge_z_positions))

    for zidx, zpos in enumerate(charge_z_positions):
        charge_heights[zidx] = charge_z_positions[zpos] + 150

        control = np.load(join(out_folder, 'fem_control_%s_mea_plane.npy' % zpos)) / normalize_constant
        control_rms[zidx] = np.sqrt(np.average(control**2))
        for param_set in param_sets:
            name = param_set['set_name']
            sim_name = join(out_folder, 'fem_%s_%s' % (name, zpos))
            if not os.path.isfile('%s_mea_plane.npy' % sim_name):
                print "No FEM of %s. Using MoI" % sim_name
                sim_name = join(out_folder, 'moi_%s_%s' % (name, zpos))
            mea_plane = np.load('%s_mea_plane.npy' % sim_name) / normalize_constant
            # print name, charge_heights[zidx],
            errors[name][zidx] = rms_arrays(mea_plane, control)
            rel_max_diff[name][zidx] = np.max(np.abs(mea_plane - control)) / np.max(np.abs(control))
            print zpos, np.max(np.abs(mea_plane - control)), np.max(np.abs(control)), rel_max_diff[name][zidx]
            rms[name][zidx] = np.sqrt(np.average(mea_plane**2))

    plt.close('all')
    fig = plt.figure(figsize=[4.2, 2.6])
    fig.subplots_adjust(bottom=0.5, top=0.88, wspace=0.7)
    yscale = 'log'
    ax1 = fig.add_subplot(131, xlabel='Source height [$\mu m]$', ylabel='RMS', yscale=yscale,
                          xlim=[-5, 300], ylim=[1e-2, 2])
    ax2 = fig.add_subplot(132, xlabel='Source height [$\mu m]$', ylabel='Relative RMS difference',
                          yscale=yscale, xlim=[-5, 300], ylim=[1e-2, 1e0])
    ax3 = fig.add_subplot(133, xlabel='Source height [$\mu m]$', ylabel='Relative max difference',
                          yscale=yscale, xlim=[-5, 300], ylim=[1e-2, 1e0])
    argsort = np.argsort(charge_heights)

    lines = []
    line_names = []
    l0, = ax1.plot(charge_heights[argsort], control_rms[argsort],
                  '-', c='k', lw=2, markersize=3, mec='k', zorder=10)
    lines.append(l0)
    line_names.append('300 $\mu m$ thick slice')
    for param_set in param_sets:
        l, = ax1.plot(charge_heights[argsort], rms[param_set['set_name']][argsort], '-', clip_on=False,
                      color=param_set['clr'], lw=2, markersize=3, mec=param_set['clr'])
        if not param_set['set_name'] is 'a':
            ax2.plot(charge_heights[argsort], errors[param_set['set_name']][argsort] / control_rms[argsort],
                      '-', color=param_set['clr'], lw=2, markersize=3, mec=param_set['clr'], clip_on=False)
            ax3.plot(charge_heights[argsort], rel_max_diff[param_set['set_name']][argsort],
                      '-', color=param_set['clr'], lw=2, markersize=3, mec=param_set['clr'])
        lines.append(l)
        line_names.append(param_set['description'])

    mark_subplots([ax1, ax2, ax3])
    simplify_axes([ax1, ax2, ax3])
    ax1.grid(True)
    ax2.grid(True)
    ax3.grid(True)
    [ax.set_xticks([0, 100, 200, 300]) for ax in [ax1, ax2, ax3]]
    fig.legend(lines, line_names, frameon=False, ncol=2, loc='lower center')
    fig.savefig('figure_7.png', dpi=150)
    # fig.savefig('PS_errors.pdf', dpi=150)

def fem_simulations():

    out_folder = 'point_source_results'
    df.parameters["krylov_solver"]["relative_tolerance"] = 1e-10
    if not os.path.isdir(out_folder):
        os.mkdir(out_folder)

    param_sets = [set_control, set_a, set_d, set_anis2, set_k, set_f, set_sl1, set_sl5]
    print "Loading meshes..."
    mesh = df.Mesh(join(mesh_dict['folder'], "mesh_E0.xml"))
    subdomains = df.MeshFunction("size_t", mesh, join(mesh_dict['folder'], "mesh_E0_physical_region.xml"))
    boundaries = df.MeshFunction("size_t", mesh, join(mesh_dict['folder'], "mesh_E0_facet_region.xml"))
    V = df.FunctionSpace(mesh, "CG", 2)
    Vs = df.FunctionSpace(mesh, "DG", 0) # For conductivity tensor
    for param_set in param_sets:
        conductivity_function = eval('ConductivityClass.%s' % param_set['class_name'])
        sigma_class = conductivity_function(**param_set)
        sigma = sigma_class.return_conductivity_tensor(Vs)
        #plot_conductivity(sigma, param_set)
        for zpos_name in charge_z_positions:
            print "Running set %s with postition %s " % (param_set['set_name'], zpos_name)
            fem_simulation(param_set, zpos_name, mesh, boundaries, subdomains, V, sigma, out_folder)

if __name__ == '__main__':

    fem_simulations()
    make_figure_4()
    make_figure_6()
    make_figure_7()
    make_figure_8()
