from os.path import join
import pickle
from plotting_convention import *

def draw_traub(ax0):

    x_shift = 1.5
    y_shift = 0.5
    l4 = 0.3 + x_shift
    l5 = 0. + x_shift
    l6 = -0.5 + x_shift

    ax0.plot([-0.5 + y_shift, 0.5 + y_shift], [l4, l4], color='k', lw=1.2)
    ax0.plot([-0.5 + y_shift, 0.5 + y_shift], [l5, l5], color='k', lw=1.2)
    ax0.plot([-0.5 + y_shift, 0.5 + y_shift], [l6, l6], color='k', lw=1.2)

    pkl_file = open(join('traub_data', '300_0_f', 'point_pos_dict.pkl'), 'rb')
    data1 = pickle.load(pkl_file)
    y, x = np.array([[d[1], d[2]] for d in data1.values()]).T / 1000.
    ax0.scatter(y + y_shift, x + x_shift, c='gray', edgecolor='none', s=0.1, zorder=0)

    ax0.text(0.6 + y_shift, l4 + 0.3, 'L2/3')
    ax0.text(0.6 + y_shift, l4 - 0.2, 'L4')
    ax0.text(0.6 + y_shift, l5 - 0.32, 'L5')
    ax0.text(0.6 + y_shift, l6 - 0.5, 'L6')

    ax0.set_xlabel(r'y $\rightarrow$', size=12)
    ax0.set_ylabel(r'x $\rightarrow$', size=12, ha='center')
    ax0.plot([-0.46 + y_shift, -0.16 + y_shift], [-1.43 + x_shift, -1.43 + x_shift], 'r', lw=2, clip_on=False)
    ax0.text(-0.45 + y_shift, -1.37 + x_shift, '300 $\mu m$', clip_on=False, color='r')


def make_figure_13():

    param_sets = {'a': 'Semi-infinite slice', 'b': 'Isotropic slice', 'c': 'Inhomogeneous\nand anisotropic',
                  'e': 'Saline layer on MEA\nplane 30 $\mu m$ thick', 'numeric': 'Infinite homogeneous'}

    displacement = '0'
    folder = join('traub_data', '300_%s_f' % displacement)
    frame_number = 1210

    plot_list = ['b', 'a', 'c', 'e']
    phi_MEA = {}
    mea_max = []

    for set_name in param_sets.keys():
        phi_MEA[set_name] = np.load(join(folder, 'Vms_MEA', 'total_phi_%s.npy' % set_name))[:, frame_number]/1000.
        mea_max.append(np.max(np.abs(phi_MEA[set_name])))

    phi_CS = {}
    cs_max = []
    for set_name in param_sets.keys():
        try:
            phi_CS[set_name] = np.load(join(folder, 'Vms_CS', 'total_phi_%s.npy' % set_name))[:, frame_number]/1000.
            cs_max.append(np.max(np.abs(phi_CS[set_name])))
        except IOError:
            print 'No value for ', set_name
            pass

    mea_max = 0.41
    cs_max = 0.105

    xi = 1000*np.load(join(folder, 'Vms_MEA', 'surface_y_ele.npy'))
    yi = 1000*np.load(join(folder, 'Vms_MEA', 'surface_z_ele.npy'))
    xi_cs = 1000*np.load(join(folder, 'Vms_CS', 'cs_x_ele.npy'))
    yi_cs = 1000*np.load(join(folder, 'Vms_CS', 'cs_z_ele.npy'))

    clr_map = plt.cm.PRGn

    levels_mea = np.linspace(-1, 1, 16) * mea_max
    levels_cs = np.linspace(-1, 1, 16) * cs_max
    plt.rcParams['contour.negative_linestyle'] = 'solid'
    fig = plt.figure(figsize=[6.4, 2.7])

    ax_height = 0.5
    ax_width = 0.08
    ax1_start = 0.05
    ax2_start = 0.3
    ax3_start = 0.3
    compare_to_MoI_dict = {'b': 'control',
                           'a': 'no_saline',
                           }
    ax_markers = list('bcdefghijklmnmno')

    ax0 = fig.add_axes([0.025, ax2_start, ax_width, ax_height], title='Slice', xticks=[], yticks=[], aspect='equal',
                          ylim=[0, 3], xlim=[0, 1])

    cax_1 = fig.add_axes([0.9, 0.32, 0.01, 0.4], title='$mV$')
    cax_2 = fig.add_axes([0.77, 0.05, 0.17, 0.015], ylabel='$mV$')
    draw_traub(ax0)
    mark_subplots(ax0)

    for row, set_name in enumerate(plot_list):

        ax_x_start = .65 * row / (len(plot_list) - 1) + 0.12
        fig.text(ax_x_start + 0.08, 0.9, param_sets[set_name], size=10, va='center', ha='center')
        ax1 = fig.add_axes([ax_x_start, ax1_start, 0.17, 0.2],
                           xticks=[], yticks=[], aspect='equal')
        ax2 = fig.add_axes([ax_x_start + 0.01, ax2_start, ax_width, ax_height],
                           aspect='equal', xticks=[], yticks=[])

        mark_subplots(ax2, ax_markers.pop(0), ypos=1.06, xpos=0.0)
        if set_name in compare_to_MoI_dict:
            ax3 = fig.add_axes([ax_x_start + 0.09, ax3_start, ax_width, ax_height],
                            aspect='equal',
                            xticks=[], yticks=[])
            ax3.set_title('MoI')
            if set_name == 'b':
                moi_name = join(folder, 'Vms_MEA', 'total_phi_moi.npy')
                moi = np.load(moi_name)[:, frame_number].T / 1000.
            elif set_name == 'a':
                moi = 2 * phi_MEA['numeric']
            # moi /= mea_max
            mark_subplots(ax3, ax_markers.pop(0), ypos=1.06, xpos=0.1)
            imcf = ax3.contourf(xi, yi, moi.reshape(30, 10), levels=levels_mea, vmin=-mea_max, vmax=mea_max,
                                cmap=clr_map, rasterized=True, extend='both')
            ax3.contour(xi, yi, moi.reshape(30, 10), levels=levels_mea, colors='k', linewidths=np.array([0.3]))
        mark_subplots(ax1, ax_markers.pop(0), ypos=1.2, xpos=0.0)

        zi = phi_MEA[set_name].reshape(30, 10)
        imcf = ax2.contourf(xi, yi, zi, levels=levels_mea, cmap=clr_map, rasterized=True)
        ax2.contour(xi, yi, zi, levels=levels_mea, colors='k', linewidths=np.array([0.3]),
                    vmin=-mea_max, vmax=mea_max, extend='both')

        if set_name in phi_CS:
            zi_cs = phi_CS[set_name].reshape(30, 10)
            imcf_cs = ax1.contourf(yi_cs, xi_cs, zi_cs, levels=levels_cs, cmap=clr_map,
                                   rasterized=True, vmin=-cs_max, vmax=cs_max)
            ax1.contour(yi_cs, xi_cs, zi_cs, levels=levels_cs, colors='k', linewidths=np.array([0.3]))
        ax2.set_title('FEM')

        if set_name == 'b':
            ax1.plot([-1500, 1500], [150, 150], color='k', lw=1)
            ax1.set_xlabel(r'x $\rightarrow$', size=12)
            ax1.set_ylabel(r'z $\rightarrow$', size=12)
        elif set_name == 'c':
            ax1.plot([-500, -500], [-150, 150], color='k', lw=1, ls='--')
            ax1.plot([0., 0.], [-150, 150], color='k', lw=1, ls='--')
            ax1.plot([300, 300], [-150, 150], color='k', lw=1, ls='--')
            ax1.plot([-1500, 1500], [150, 150], color='k', lw=1)
            ax2.plot([-500, 500], [-500, -500], color='k', lw=1, ls='--')
            ax2.plot([-500, 500], [0., 0.], color='k', lw=1, ls='--')
            ax2.plot([-500, 500], [300, 300], color='k', lw=1, ls='--')
        elif set_name == 'd':
            ax2.plot([-1500, 1500], [150, 150], color='k', lw=1)
            ax1.plot([-1500, 1500], [-140, -140], color='k', lw=1, ls='-.')
        elif set_name == 'e':
            ax1.plot([-1500, 1500], [150, 150], color='k', lw=1)
            ax1.plot([-1500, 1500], [-120, -120], color='k', lw=1, ls='-.')
            clrbar1 = plt.colorbar(imcf, cax=cax_1)
            clrbar1.set_ticks([-0.4, -0.2, 0, 0.2, 0.4])
            # clrbar1.set_label('mV')
            clrbar2 = plt.colorbar(imcf_cs, cax=cax_2, orientation='horizontal')
            clrbar2.set_ticks([-0.1, -0.05, 0, 0.05, 0.1])
        elif 'noise' in set_name:
            ax1.plot([-1500, 1500], [150, 150], color='k', lw=1)

    fig.savefig('figure_13.png', dpi=150)

if __name__ == '__main__':
    make_figure_13()