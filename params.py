
import numpy as np

slice_thickness = 300.
a = slice_thickness/2.
elec_z = -slice_thickness/2.
sigma_T = 0.3
sigma_S = 1.5

mesh_dict = {'folder': 'mesh',
             'meshElectrode': 1.,
             'meshInnerRetina': 2.5,
             'meshRetina': 500.,
             'meshSaline': 1500.,
             'cylinder_radius': 8000.,
             'cylinder_height': 8000.,
             'retina_thickness': 300.,
             'retina_radius': 8000.,
             'inner_retina_radius': 20.,
             'inner_retina_thickness': 300.,
             'elec_r1': 5.,
             'elec_r2': 10.,
             'elec_r3': 15.,
             'elec_depth': 10.
             }

numcols_cs = 201
numrows_cs = 100
#x_extent_cs = 500
x_extent_cs = 1000

epsilon = 1e-6
numrows_mea = 101
extent_mea = 1000

crossection_x, crossection_z = np.meshgrid(np.linspace(-x_extent_cs, x_extent_cs, numcols_cs), 
                                           np.linspace(-a + epsilon, 3*a - epsilon, numrows_cs))
plane_x, plane_y = np.meshgrid(np.linspace(-extent_mea, extent_mea, numrows_mea), 
                               np.linspace(-extent_mea, extent_mea, numrows_mea))

charge_z_positions = {'za': -149.,
                      'zb': -148.,
                      'zc': -145.,
                      'zd': -140.,
                      'ze2': -139.,
                      'ze3': -135.,
                      'ze': -130.,
                      'zf': -119.,
                      'zg': -118.,
                      'zh': -115.,
                      'zi': -110.,
                      'zj': -105.,
                      'z0': -100.,
                      'z1': -50.,
                      'z2': 0.,
                      'z3': 50.,
                      'z4': 100.,
                      'z5': 130.,
                      'z6': 140.,
                      'z7': 145.,
                      'z8': 148.,
                      'z9': 149.
                      }
set_control = {
    'class_name': 'ControlConductivity',
    'set_name': 'control',
    'description': 'Isotropic slice\n$\sigma_{T}$ = 0.3 S/m',
    'sigma_T': sigma_T, # Tissue
    'sigma_S': sigma_S, # Saline
    'slice_thickness': slice_thickness,
    'clr': 'k',
    }

set_a = {
    'class_name': 'InfHomoConductivity',
    'set_name': 'a',
    'description': 'Semi-infinite slice',
    'sigma_T': sigma_T,
    'sigma_S': sigma_T,
    'slice_thickness': slice_thickness,
    'clr': 'r',
    }

set_sl1 = {
    'class_name': 'SalineLayer',
    'set_name': 'b',
    'description': 'Saline interface\nthickness 10 $\mu m$',
    'interface_thickness': 10,
    'sigma_T': sigma_T,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'b',
    }


set_sl3 = {
    'class_name': 'SalineLayer',
    'set_name': 'sl3',
    'description': 'Saline layer on MEA plane\n20 $\mu m$ thick',
    'interface_thickness': 20,
    'sigma_T': sigma_T,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'orange',
    }


set_sl5 = {
    'class_name': 'SalineLayer',
    'set_name': 'c',
    'description': 'Saline layer on MEA plane\n30 $\mu m$ thick',
    'interface_thickness': 30,
    'sigma_T': sigma_T,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'g',
    }


set_sl5b = {
    'class_name': 'SalineLayerSimplified',
    'set_name': 'cb',
    'description': 'Saline layer on MEA plane\n30 $\mu m$ thick',
    'interface_thickness': 30,
    'sigma_T': sigma_T,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'g',
    }

set_d = {
    'class_name': 'Anisotropic',
    'set_name': 'd',
    'description': 'Anisotropic slice\n$\sigma_{x}$ = 0.45 S/m, $\sigma_{y,z}$ = 0.3 S/m',
    'sigma_T': np.array([0.45, 0.3, 0.3]),
    'sigma_S': 1.5,
    'slice_thickness': slice_thickness,
    'clr': 'm',
    }

set_e = {
    'class_name': 'Anisotropic',
    'set_name': 'e',
    'description': 'Anisotropic slice\n$\sigma_{x}$ = 0.4 S/m, $\sigma_{y,z}$ = 0.2 S/m',
    'sigma_T': np.array([0.4, 0.2, 0.2]),
    'sigma_S': 1.5,
    'slice_thickness': slice_thickness,
    'clr': 'y',
    }


set_anis2 = {
    'class_name': 'Anisotropic',
    'set_name': 'anis2',
    'description': 'Anisotropic slice\n$\sigma_{x}$ = 0.6 S/m, $\sigma_{y,z}$ = 0.3 S/m',
    'sigma_T': np.array([0.6, 0.3, 0.3]),
    'sigma_S': 1.5,
    'slice_thickness': slice_thickness,
    'clr': 'y',
    }

set_f = {
    'class_name': 'Inhomogeneous',
    'set_name': 'f',
    'description': 'Inhomogeneous slice\n$\sigma_{T1}$ = 0.3 S/m, $\sigma_{T2}$ = 0.4 S/m',
    'sigma_T1': 0.3,
    'sigma_T2': 0.4,
    #'sigma_T3': 0.4,
    'inhomo_x_pos': 20,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'gray'
    }

set_g = {
    'class_name': 'Inhomogeneous',
    'set_name': 'g',
    'description': 'Edge effect',
    'sigma_T1': sigma_T,
    'sigma_T2': sigma_S,
    'inhomo_x_pos': slice_thickness,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'pink'
    }

set_i = {
    'set_name': 'i',
    'description': 'Infinite\nhomogeneous',
    'sigma_T': sigma_T,
    'clr': 'orange'
    }

set_j = {
    'class_name': 'Inhomogeneous',
    'set_name': 'j',
    'description': 'Inhomogeneous slice\n$\sigma_{T1}$ = 0.2 S/m, $\sigma_{T2}$ = 0.4 S/m',
    'sigma_T1': 0.2,
    'sigma_T2': 0.4,
    #'sigma_T3': 0.4,
    'inhomo_x_pos': 20,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': '0.4'
    }

set_k = {
    'class_name': 'Inhomogeneous',
    'set_name': 'k',
    'description': 'Inhomogeneous slice\n$\sigma_{T1} = 0.3$ S/m, $\sigma_{T2}$ = 0.5 S/m',
    'sigma_T1': 0.3,
    'sigma_T2': 0.5,
    #'sigma_T3': 0.4,
    'inhomo_x_pos': 20,
    'sigma_S': sigma_S,
    'slice_thickness': slice_thickness,
    'clr': 'orange'
    }

set_electrode = {
    'class_name': 'ElectrodeConductivity',
    'set_name': 'electrode',
    'description': 'Electrode',
    'sigma_T': 0.3,
    'sigma_S': 1.5,
    'sigma_E': 1e5, # Platinum electrode
    'slice_thickness': slice_thickness,
    'clr': 'k',
    }