#!/usr/bin/env python

import numpy as np
from os.path import join
import LFPy
import ConductivityClass
import neuron
import pylab as plt
import ViMEAPy
from plotting_convention import *
from params import *

model = 'hay'
neuron_model = join('hay_model', 'lfpy_version')


class Electrode:
    """ Class to make the electrodes.
    """
    def __init__(self, slice_thickness=300., n_rows=12, n_cols=8, elec_sep=100., elec_radius=15.):
        self.slice_thickness = slice_thickness
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.num_elecs = n_rows * n_cols
        self.elec_sep = elec_sep
        self.elec_radius = elec_radius
        self.elec_x = np.zeros(self.num_elecs)
        self.elec_y = np.zeros(self.num_elecs)
        self.elec_z = - slice_thickness/2.
        i = 0
        for col in xrange(n_cols):
            for row in xrange(n_rows):
                x_pos = col*elec_sep - (n_cols-1)/2. * elec_sep
                y_pos = (n_rows -row)*elec_sep - (n_rows+1)/2. * elec_sep
                self.elec_x[i], self.elec_y[i] = x_pos, y_pos
                i += 1
    
def make_mappings(folder, cell, MEA, ext_sim_dict, zpos=0):
    moi_normal_saline = {
        'sigma_G': 0.0,  # Below electrode
        'sigma_T': 0.3,  # Tissue
        'sigma_S': 1.5,  # Saline
        'h': MEA.slice_thickness,
        'steps': 20,
        }

    moi_saline_as_tissue = {
        'sigma_G': 0.0,  # Below electrode
        'sigma_T': 0.3,  # Tissue
        'sigma_S': 0.3,  # Saline
        'h': MEA.slice_thickness,
        'steps': 3,
        }

    h = moi_saline_as_tissue['h']  # Correct for different conventions!
    moi_normal_saline = ViMEAPy.MoI(**moi_normal_saline)
    moi_saline_as_tissue = ViMEAPy.MoI(**moi_saline_as_tissue)

    cell.set_pos(xpos=0, ypos=-500, zpos=zpos)
    mapping_saline_as_tissue = moi_saline_as_tissue.make_mapping_cython(
        ext_sim_dict, xmid=cell.xmid, ymid=cell.ymid, zmid=cell.zmid + h/2., xstart=cell.xstart,
        ystart=cell.ystart, zstart=cell.zstart + h/2., xend=cell.xend, yend=cell.yend, zend=cell.zend + h/2.)

    mapping_normal_saline = moi_normal_saline.make_mapping_cython(
        ext_sim_dict, xmid=cell.xmid, ymid=cell.ymid, zmid=cell.zmid + h/2., xstart=cell.xstart,
        ystart=cell.ystart, zstart=cell.zstart + h/2., xend=cell.xend, yend=cell.yend, zend=cell.zend + h/2.)

    print cell.zmid

    np.save(join(folder, 'mapping_saline_as_tissue_%d.npy' % zpos), mapping_saline_as_tissue)
    np.save(join(folder, 'mapping_normal_saline_%d.npy' % zpos), mapping_normal_saline)
    
def return_cell(folder, MEA, ext_sim_dict, zpos, simulate_cell=True, make_mapping=True):

    time_res = 2**-5
    total_t = 200
    cell_params = {
        'morphology': join(neuron_model, 'cell1.hoc'),
        #'rm' : 30000,               # membrane resistance
        #'cm' : 1.0,                 # membrane capacitance
        #'Ra' : 100,                 # axial resistance
        'v_init': -70,             # initial crossmembrane potential
        #'e_pas' : -90,              # reversal potential passive mechs
        'passive': False,           # switch on passive mechs
        'nsegs_method': 'lambda_f',# method for setting number of segments,
        'lambda_f': 100,           # segments are isopotential at this frequency
        'timeres_NEURON': time_res,   # dt of LFP and NEURON simulation.
        'timeres_python': time_res,
        'tstartms': -500,          #start time, recorders start at t=0
        'tstopms': total_t,           #stop time of simulation
        'custom_code': [join(neuron_model, 'custom_codes.hoc'),
                        join(neuron_model, 'biophys3_active.hoc')],
        }

    print "Making cell"
    neuron.h('forall delete_section()')
    neuron.load_mechanisms(join(neuron_model, '..', 'mod'))
    cell = LFPy.Cell(**cell_params)
    for sec in cell.allseclist:
        for seg in sec:
            seg.e_pas = -55
    cell.set_rotation(x=-np.pi/2, y=-np.pi/10)    
    cell.set_pos(xpos=0, ypos=-500, zpos=zpos)
    cell.zmid = (cell.zmid - zpos) / 2. + zpos
    cell.zstart = (cell.zstart - zpos) / 2. + zpos
    cell.zend = (cell.zend - zpos) / 2. + zpos
    if simulate_cell:
        cell.simulate(rec_imem=True)
        np.save(join(folder, 'imem.npy'), cell.imem)
        np.save(join(folder, 'tvec.npy'), cell.tvec)
        np.save(join(folder, 'somav.npy'), cell.somav)
    else:
        cell.imem = np.load(join(folder, 'imem.npy'))
        cell.somav = np.load(join(folder, 'somav.npy'))
        cell.tvec = np.load(join(folder, 'tvec.npy'))
    if make_mapping:
        make_mappings(folder, cell, MEA, ext_sim_dict, zpos=zpos)
    return cell


def plot_cell_to_2_axis(cell, ax_neur, side_ax, MEA, zpos, elec_idxs):

    text_clr = 'b'
    text_size = 7

    for comp in xrange(len(cell.xmid)):
        if comp == 0:
            ax_neur.scatter(cell.xmid[comp], cell.ymid[comp], s=cell.diam[comp], 
                            edgecolor='none', color='gray', zorder=1)
        else:
            ax_neur.plot([cell.xstart[comp], cell.xend[comp]],
                         [cell.ystart[comp], cell.yend[comp]], 
                         lw=cell.diam[comp]/2, color='gray', zorder=1)

    cell.set_pos(xpos=0, ypos=-500, zpos=zpos)
    for comp in xrange(len(cell.xmid)):
        if comp == 0:
            side_ax.scatter(cell.ymid[comp], cell.zmid[comp], s=cell.diam[comp], 
                            edgecolor='none', color='gray', zorder=1)
        else:    
            side_ax.plot([cell.ystart[comp], cell.yend[comp]],
                         [cell.zstart[comp], cell.zend[comp]],
                         lw=cell.diam[comp]/2, color='gray', zorder=1)

    elec_letters = ['VI', 'V', 'IV', 'III', 'II', 'I']
    for elec in elec_idxs:
        elec_name = elec_letters.pop()
        side_ax.text(MEA.elec_y[elec], MEA.elec_z - 30, elec_name,
                     va='top', ha='center', color=text_clr, size=text_size)

        ax_neur.text(MEA.elec_x[elec] - 20, MEA.elec_y[elec] - 30, elec_name,
                     va='top', ha='center', color=text_clr, size=text_size)


    ax_neur.arrow(100, 100, 80, 0, lw=1, head_width=30, color='k')
    ax_neur.arrow(100, 100, 0, 100, lw=1, head_width=30, color='k')
    ax_neur.text(100, 290, 'x', size=10, ha='center', va='center')
    ax_neur.text(250, 100, 'y', size=10, ha='center', va='center')


    side_ax.arrow(-1200, -50, 80, 0, lw=1, head_width=30, color='k', clip_on=False)
    side_ax.text(-1000, -50, 'x', size=10, ha='center', va='center')
    side_ax.arrow(-1200, -50, 0, 100, lw=1, head_width=30, color='k', clip_on=False)
    side_ax.text(-1200, 180, 'z', size=10, ha='center', va='center')

    # side_ax.text(-1300, -90, r'$\rightarrow$ x', size=10, ha='left', va='center')
    # side_ax.text(-1300, -115, r'$\rightarrow$ z', rotation='vertical', size=10, ha='left', va='bottom')

    side_ax.plot([-950, 950], [-MEA.elec_z, -MEA.elec_z], color='k')
    side_ax.plot([-950, 950], [MEA.elec_z, MEA.elec_z], 'k')    
    ax_neur.axis([-300, 300, -700, 800])
    side_ax.axis([-1000, 1000, -200, 200])
    side_ax.plot([1000, 1000], [-MEA.slice_thickness/2, MEA.slice_thickness/2],
                 color='k', lw=2, clip_on=False)
    side_ax.text(1050, 0, '%g $\mu m$' % MEA.slice_thickness, size=8, va='center')
    
        
def make_figure_11(cell, MEA, zpos):
    """
    SAT : Saline As Tissue
    NS: Normal Saline
    """
    zpos_name_dict = {-100: 'A', 0: 'B', 100: 'C'}

    spike_t_range = [155, 165]
    elec_idxs = [23, 22, 20, 11, 18, 24]
    idx1 = np.argmin(np.abs(cell.tvec - spike_t_range[0]))
    idx2 = np.argmin(np.abs(cell.tvec - spike_t_range[1]))
    cell.tvec = cell.tvec[idx1:idx2] - cell.tvec[idx1]
    cell.imem = cell.imem[:, idx1:idx2]
    cell.somav = cell.somav[idx1:idx2]

    sig_NS  = 1000 * np.dot(np.load(join(folder, 'mapping_normal_saline_%d.npy' % zpos)), cell.imem)
    sig_SAT = 1000 * np.dot(np.load(join(folder, 'mapping_saline_as_tissue_%d.npy' % zpos)), cell.imem)

    plt.close('all')
    fig = plt.figure(figsize=[3, 3])
    
    ax_neur = fig.add_axes([0.33, 0.25, 0.33, 0.65], frameon=False, xticks=[], yticks=[])
    side_ax = fig.add_axes([0.2, 0.017, 0.6, 0.2], frameon=False, xticks=[], yticks=[], aspect='equal')
    plot_cell_to_2_axis(cell, ax_neur, side_ax, MEA, zpos, elec_idxs)
    mark_subplots(ax_neur, zpos_name_dict[int(zpos)], ypos=1.1, xpos=-0.9)

    neur_ax = ax_neur.axis()

    sig_NS = sig_NS[elec_idxs, :]
    sig_SAT = sig_SAT[elec_idxs, :]
    elec_x = MEA.elec_x[elec_idxs]
    elec_y = MEA.elec_y[elec_idxs]
    ext_ax_width = 0.25
    ext_ax_height = 0.15
    ax_heights = [0.25, 0.25, 0.5, 0.5, 0.75, 0.75]
        
    for elec in xrange(len(elec_x)):
        if elec in [5, 1, 2]:
            ax_xpos = 0.7
        else:
            ax_xpos = 0.08
        ax_neur.scatter(elec_x[elec], elec_y[elec], s=5, edgecolor='none', c='k', zorder=100, clip_on=False)
        side_ax.scatter(elec_y[elec], MEA.elec_z, s=5, edgecolor='none', c='k', zorder=100, clip_on=False)
        ax_temp = fig.add_axes([ax_xpos, ax_heights[elec], ext_ax_width, ext_ax_height], 
                               xticks=[])
        ax_neur.axis(neur_ax)
        simplify_axes(ax_temp)
        
        if elec == len(elec_x) - 2:
            ax_temp.plot(cell.tvec, sig_SAT[elec], '-', color='r', lw=1, clip_on=False, label='Semi infinite slice')

            ax_temp.plot(cell.tvec, sig_NS[elec], '-', color='k', lw=1, clip_on=False,
                         label='300 $\mu m$ thick slice')
            ax_temp.legend(bbox_to_anchor=(.6, 1.4),
                           ncol=1, mode="expand", borderaxespad=0., prop={'size': 6}, frameon=False)

        else:
            ax_temp.plot(cell.tvec, sig_SAT[elec], '-', color='r', lw=1, clip_on=False)
            ax_temp.plot(cell.tvec, sig_NS[elec], '-', color='k', lw=1, clip_on=False)

        ax_temp.set_yticks(ax_temp.get_ylim())

        pos = [elec_x[elec], elec_y[elec]]

        if elec == len(elec_x) - 2:
            ax_temp.set_xticks([0, 10])
            ax_temp.set_xlabel('ms')
            ax_temp.text(-3, 0.2, '$\mu V$', rotation='vertical', size=9)
        LFP_arrow_to_axis(pos, ax_neur, ax_temp, ax_xpos)
    ax_neur.axis(neur_ax)
    fig.savefig(join('figure_11_%s.png' % zpos_name_dict[int(zpos)]), dpi=150)
    # fig.savefig(join('waveform_impact.pdf'))

def LFP_arrow_to_axis(pos, ax_origin, ax_target, ax_xpos):
    if ax_xpos < 0.5:
        upper_pixel_coor = ax_target.transAxes.transform(([1, 0.5]))
    else:
        upper_pixel_coor = ax_target.transAxes.transform(([0, 0.5]))
    upper_coor = ax_origin.transData.inverted().transform(upper_pixel_coor)

    upper_line_x = [pos[0], upper_coor[0]]
    upper_line_y = [pos[1], upper_coor[1]]
    
    ax_origin.plot(upper_line_x, upper_line_y, lw=1, clip_on=False, alpha=1., color='k')


def make_figure_12(cell, MEA, zpos):
    """
    SAT : Saline As Tissue
    NS: Normal Saline
    """

    zpos_name_dict = {-100: 'A', 0: 'B', 100: 'C'}

    spike_t_range = [155, 165]
    elec_idxs = [23, 22, 20, 11, 18, 24]
    idx1 = np.argmin(np.abs(cell.tvec - spike_t_range[0]))
    idx2 = np.argmin(np.abs(cell.tvec - spike_t_range[1]))
    cell.tvec = cell.tvec[idx1:idx2] - cell.tvec[idx1]
    cell.imem = cell.imem[:, idx1:idx2]
    cell.somav = cell.somav[idx1:idx2]

    sig_NI  = 1000 * np.dot(np.load(join(folder, 'interface_control.npy')), cell.imem)
    sig_I1 = 1000 * np.dot(np.load(join(folder, 'interface_b.npy')), cell.imem)
    sig_I2 = 1000 * np.dot(np.load(join(folder, 'interface_c.npy')), cell.imem)

    plt.close('all')
    fig = plt.figure(figsize=[3, 3])

    ax_neur = fig.add_axes([0.33, 0.25, 0.33, 0.65], frameon=False, xticks=[], yticks=[])
    side_ax = fig.add_axes([0.2, 0.017, 0.6, 0.2], frameon=False, xticks=[], yticks=[], aspect='equal')
    plot_cell_to_2_axis(cell, ax_neur, side_ax, MEA, zpos, elec_idxs)

    neur_ax = ax_neur.axis()

    sig_NI = sig_NI[elec_idxs, :]
    sig_I1 = sig_I1[elec_idxs, :]
    sig_I2 = sig_I2[elec_idxs, :]
    elec_x = MEA.elec_x[elec_idxs]
    elec_y = MEA.elec_y[elec_idxs]
    ext_ax_width = 0.25
    ext_ax_height = 0.15
    ax_heights = [0.25, 0.25, 0.5, 0.5, 0.75, 0.75]

    for elec in xrange(len(elec_x)):
        if elec in [5, 1, 2]:
            ax_xpos = 0.7
        else:
            ax_xpos = 0.1
        ax_neur.scatter(elec_x[elec], elec_y[elec], s=5, edgecolor='none', c='k', zorder=100, clip_on=False)
        side_ax.scatter(elec_y[elec], MEA.elec_z, s=5, edgecolor='none', c='k', zorder=100, clip_on=False)
        ax_temp = fig.add_axes([ax_xpos, ax_heights[elec], ext_ax_width, ext_ax_height], xticks=[])
        ax_neur.axis(neur_ax)
        simplify_axes(ax_temp)

        if elec == len(elec_x) - 2:
            ax_temp.plot(cell.tvec, sig_I1[elec], '-', color='r', lw=1, clip_on=False, label='Thickness: 10 $\mu m$')
            ax_temp.plot(cell.tvec, sig_I2[elec], '-', color='g', lw=1, clip_on=False, label='Thickness: 30 $\mu m$')
            ax_temp.plot(cell.tvec, sig_NI[elec], '-', color='k', lw=1, clip_on=False,
                         label='No saline layer')
            ax_temp.legend(bbox_to_anchor=(.6, 1.6),
                           ncol=1, mode="expand", borderaxespad=0., prop={'size': 6}, frameon=False)
        else:
            ax_temp.plot(cell.tvec, sig_NI[elec], '-', color='k', lw=1, clip_on=False)
            ax_temp.plot(cell.tvec, sig_I1[elec], '-', color='r', lw=1, clip_on=False)
            ax_temp.plot(cell.tvec, sig_I2[elec], '-', color='g', lw=1, clip_on=False)

        print elec, np.max(sig_NI[elec]) - np.min(sig_NI[elec]), np.max(sig_I1[elec]) - np.min(sig_I1[elec]), np.max(sig_I2[elec]) - np.min(sig_I2[elec])

        ax_temp.set_yticks(ax_temp.get_ylim())
        pos = [elec_x[elec], elec_y[elec]]

        if elec == len(elec_x) - 2:
            ax_temp.set_xticks([0, 10])
            ax_temp.set_xlabel('ms')
            ax_temp.text(-3, 0.13, '$\mu V$', rotation='vertical', size=9)
        LFP_arrow_to_axis(pos, ax_neur, ax_temp, ax_xpos)
    ax_neur.axis(neur_ax)
    fig.savefig(join('figure_12.png'), dpi=150)
    # fig.savefig(join('waveform_impact.pdf'))


def saline_interface_fem(folder, cell, MEA):
    """ Here the Finite Element calculations are done"""
    import dolfin as df

    df.parameters["krylov_solver"]["relative_tolerance"] = 1e-10

    param_sets = [set_sl5]#[set_control, set_sl1]#set_sl3, set_sl1, set_sl5]#set_b, set_c, set_f, set_j, set_k]#[set_control, set_a]
    print "Loading meshes..."
    mesh = df.Mesh(join(mesh_dict['folder'], "mesh_E0.xml"))
    subdomains = df.MeshFunction("size_t", mesh, join(mesh_dict['folder'], "mesh_E0_physical_region.xml"))
    boundaries = df.MeshFunction("size_t", mesh, join(mesh_dict['folder'], "mesh_E0_facet_region.xml"))
    V = df.FunctionSpace(mesh, "CG", 2)
    Vs = df.FunctionSpace(mesh, "DG", 0)  # For conductivity tensor
    for param_set in param_sets:
        conductivity_function = eval('ConductivityClass.%s' % param_set['class_name'])
        sigma_class = conductivity_function(**param_set)
        sigma = sigma_class.return_conductivity_tensor(Vs)
        mapping = np.zeros((MEA.num_elecs, cell.totnsegs))
        elec_idxs = [23, 22, 20, 11, 18, 24]
        for elec_num, elec_idx in enumerate(elec_idxs):
            print "Running set %s with elec_idx %d " % (param_set['set_name'], elec_idx)
            v = df.TestFunction(V)
            u = df.TrialFunction(V)
            a = df.inner(sigma * df.grad(u), df.grad(v)) * df.dx(1)

            L = df.Constant(0) * v * df.dx
            bc = df.DirichletBC(V, df.Constant(0), boundaries, 9)
            phi = df.Function(V)
            A = df.assemble(a, cell_domains=subdomains, exterior_facet_domains=boundaries)
            b = df.assemble(L, cell_domains=subdomains, exterior_facet_domains=boundaries)
            bc.apply(A, b)
            point = df.Point(MEA.elec_x[elec_idx], MEA.elec_y[elec_idx], MEA.elec_z + 1e-8)
            delta = df.PointSource(V, point)
            delta.apply(b)
            solver = df.KrylovSolver("cg", "ilu")
            solver.solve(A, phi.vector(), b)
            mapping[elec_idx, :] = [phi(cell.xmid[comp], cell.ymid[comp], cell.zmid[comp])
                                    for comp in xrange(cell.totnsegs)]
        np.save(join(folder, 'interface_%s.npy' % param_set['set_name']), mapping)

if __name__ == '__main__':

    sparse_elec_params = {'slice_thickness': 300.,
                         'n_rows': 8,
                         'n_cols': 5,
                         'elec_sep': 200.,
                         'elec_radius': 1.,
                          }
    zpos = 0
    folder = join('hay_model', 'simulation_results')
    MEA = Electrode(**sparse_elec_params)
    ext_sim_dict = {'use_line_source': True,
                    'n_elecs': MEA.num_elecs,
                    'n_avrg_points': 100,
                    'elec_radius': MEA.elec_radius,
                    'moi_steps': 20,
                    'elec_x': MEA.elec_x,
                    'elec_y': MEA.elec_y,
                    'elec_z': MEA.elec_z + MEA.slice_thickness/2,
                    'elec_sep': MEA.elec_sep,
                    'slice_thickness': MEA.slice_thickness,
                    'include_elec': False,
                    'neural_input': '.',
                    }

    cell = return_cell(folder, MEA, ext_sim_dict, zpos=zpos, simulate_cell=False,
                        make_mapping=True)
    # saline_interface_fem(folder, cell, MEA)
    make_figure_12(cell, MEA, zpos)
    # make_figure_11(cell, MEA, zpos)