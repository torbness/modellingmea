import numpy as np
import os
from os.path import join
import pylab as plt
import ViMEAPy
from plotting_convention import *
from params import *


def do_figure_9_sim():
    """ Tests the impact of the saline bath above the slice on point source. Underscore heigth '_heigth'
    labels a study of different source heights, while _decay labels a study of the decay in MEA plane.
    """

    folder = 'simulation_results'
    if not os.path.isdir(folder): os.mkdir(folder)
    sigma_Ts = [0.3, 0.3, 0.3, 0.3]
    sigma_Ss = [0.0, 0.3, 1.5, 1e3]
    np.save(join(folder, 'saline_bath_sigmas.npy'), np.array([sigma_Ts, sigma_Ss]))

    z_pos_height = np.arange(-149, 150, dtype=float)
    charge_pos_height = np.zeros((len(z_pos_height), 3))
    charge_pos_height[:, 2] = z_pos_height
    values_height = np.zeros((len(sigma_Ts), charge_pos_height.shape[0]))

    MoI_height_kwargs = {'sigma_T': None,
                  'sigma_S': None,
                  'elec_y': np.zeros(1, dtype='float'),
                  'elec_x': np.zeros(1, dtype='float'),
                  'elec_z': -slice_thickness/2.,
                  'steps': 20,
                  'xmid': np.zeros(len(z_pos_height)),
                  'ymid': np.zeros(len(z_pos_height)),
                  'zmid': np.array(z_pos_height, dtype='float'),
                  }
    for sigma_idx in xrange(len(sigma_Ts)):
        MoI_height_kwargs['sigma_T'] = sigma_Ts[sigma_idx]
        MoI_height_kwargs['sigma_S'] = sigma_Ss[sigma_idx]
        values_height[sigma_idx, :] = ViMEAPy.PS_without_elec_mapping_shifted(**MoI_height_kwargs)

    np.save(join(folder, 'charges_pos_saline_bath_effect_height.npy'), z_pos_height - elec_z)
    np.save(join(folder, 'saline_bath_effect_height.npy'), values_height)


    elec_x_decay = np.linspace(0, 600, 1000)

    for zpos in [-100., 0., 100.]:
        values_decay = np.zeros((len(sigma_Ts), len(elec_x_decay)))
        MoI_decay_kwargs = {'sigma_T': None,
                      'sigma_S': None,
                      'elec_y': np.zeros(len(elec_x_decay), dtype='float'),
                      'elec_x': elec_x_decay,
                      'elec_z': -slice_thickness/2.,
                      'steps': 20,
                      'xmid': np.zeros(1, dtype='float'),
                      'ymid': np.zeros(1, dtype='float'),
                      'zmid': np.array([zpos], dtype='float'),
                      }

        for sigma_idx in xrange(len(sigma_Ts)):
            MoI_decay_kwargs['sigma_T'] = sigma_Ts[sigma_idx]
            MoI_decay_kwargs['sigma_S'] = sigma_Ss[sigma_idx]
            values_decay[sigma_idx, :] = ViMEAPy.PS_without_elec_mapping_shifted(**MoI_decay_kwargs)[:, 0]
        np.save(join(folder, 'elec_pos_saline_bath_effect_decay_%d.npy' % zpos), elec_x_decay)
        np.save(join(folder, 'saline_bath_effect_decay_%d.npy' % zpos), values_decay)

def make_figure_9():

    folder = 'simulation_results'
    heights = np.load(join(folder, 'charges_pos_saline_bath_effect.npy'))
    values_height = np.load(join(folder, 'saline_bath_effect.npy'))
    sigma_Ts, sigma_Ss = np.load(join(folder, 'saline_bath_sigmas.npy'))

    elec_x = np.load(join(folder, 'elec_pos_saline_bath_effect_decay_0.npy'))
    values_decay_l = np.load(join(folder, 'saline_bath_effect_decay_-100.npy'))
    values_decay_c = np.load(join(folder, 'saline_bath_effect_decay_0.npy'))
    values_decay_u = np.load(join(folder, 'saline_bath_effect_decay_100.npy'))

    plt.close('all')
    fig = plt.figure(figsize=[4.5, 3.3])
    fig.subplots_adjust(left=0.12, bottom=0.2, hspace=0.7, wspace=0.6, top=0.9)
    ax1 = fig.add_subplot(231, yscale='log', xlabel='Source height [$\mu m$]',
                       ylabel='Potential', xticks=[0, 150, 300], ylim=[1e-4, 1.1], xlim=[0, 310])
    ax1b = fig.add_subplot(232, yscale='log', xlabel='Source height [$\mu m$]',
                       ylabel='Difference', xticks=[0, 150, 300], xlim=[0, 310])
    ax1c = fig.add_subplot(233, yscale='log', xlabel='Source height [$\mu m$]',
                       ylabel='Relative difference', xticks=[0, 150, 300], xlim=[0, 310])
    ax2 = fig.add_subplot(234, yscale='log', xlabel='Distance in MEA plane [$\mu m$]',
                       ylabel='Potential', xticks=[0, 200, 400, 600], ylim=[1e-2, 1.1],
                       title='Source height: 50 $\mu m$')
    ax3 = fig.add_subplot(235, yscale='log', xlabel='Distance in MEA plane [$\mu m$]',
                       ylabel='Potential', xticks=[0, 200, 400, 600], ylim=[1e-2, 1.1],
                       title='Source height: 150 $\mu m$')
    ax4 = fig.add_subplot(236, yscale='log', xlabel='Distance in MEA plane [$\mu m$]',
                       ylabel='Potential', xticks=[0, 200, 400, 600], ylim=[1e-2, 1.1],
                       title='Source height: 250 $\mu m$')

    clr = 'grky'
    [ax.grid(True) for ax in fig.axes]

    normalize_factor = np.max(values_height)

    lines = []
    names = []
    for sigma_idx in xrange(len(sigma_Ts)):
        l, = ax1.plot(heights, values_height[sigma_idx]/normalize_factor, color=clr[sigma_idx], lw=1.)
        if not sigma_idx == 2:
            ax1b.plot(heights, np.abs((values_height[2] - values_height[sigma_idx])/normalize_factor),
                      color=clr[sigma_idx], lw=1.)
            ax1c.plot(heights, np.abs((values_height[2] - values_height[sigma_idx])/values_height[2]),
                      color=clr[sigma_idx], lw=1.)

        lines.append(l)
        if sigma_Ss[sigma_idx] > 10:
            sigma_name = "$\sigma_S$ = %d S/m" % (sigma_Ss[sigma_idx])
        elif sigma_Ss[sigma_idx] == 0:
            sigma_name = "$\sigma_S$ = %d S/m" % (sigma_Ss[sigma_idx])
        else:
            sigma_name = "$\sigma_S$ = %1.1f S/m" % (sigma_Ss[sigma_idx])

        names.append(sigma_name)
        ax2.plot(elec_x, values_decay_l[sigma_idx]/np.max(values_decay_l[sigma_idx]), color=clr[sigma_idx], lw=1.)
        ax3.plot(elec_x, values_decay_c[sigma_idx]/np.max(values_decay_c[sigma_idx]), color=clr[sigma_idx], lw=1.)
        ax4.plot(elec_x, values_decay_u[sigma_idx]/np.max(values_decay_u[sigma_idx]), color=clr[sigma_idx], lw=1.)

    fig.legend(lines, names, ncol=4, numpoints=4, frameon=False, loc='lower center')
    simplify_axes(fig.axes)
    mark_subplots(fig.axes, xpos=-0.2)
    plt.savefig('figure_9.png', dpi=150)

if __name__ == '__main__':
    do_figure_9_sim()
    make_figure_9()