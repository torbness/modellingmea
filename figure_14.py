import numpy as np
import os
from os.path import join
import sys
import pickle
import pylab as plt
from plotting_convention import *
import cv2


def nn(x,y):
    return np.sum(((x-y)**2).flatten()) / np.sum(x.flatten()**2)


def smooth(sig, N):
    return cv2.GaussianBlur(sig, ksize=(N, N), sigmaX=0, sigmaY=0)


def draw_traub(ax0):

    x_shift = 1.5
    y_shift = 0.5
    l4 = 0.3 + x_shift
    l5 = 0. + x_shift
    l6 = -0.5 + x_shift

    ax0.plot([-0.5 + y_shift, 0.5 + y_shift], [l4, l4], color='k', lw=1.2)
    ax0.plot([-0.5 + y_shift, 0.5 + y_shift], [l5, l5], color='k', lw=1.2)
    ax0.plot([-0.5 + y_shift, 0.5 + y_shift], [l6, l6], color='k', lw=1.2)

    pkl_file = open(join('traub_data', '300_0_f', 'point_pos_dict.pkl'), 'rb')
    data1 = pickle.load(pkl_file)
    y, x = np.array([[d[1], d[2]] for d in data1.values()]).T / 1000.
    ax0.scatter(y + y_shift, x + x_shift, c='gray', edgecolor='none', s=0.1, zorder=0)

    ax0.text(0.6 + y_shift, l4 + 0.3, 'L2/3')
    ax0.text(0.6 + y_shift, l4 - 0.2, 'L4')
    ax0.text(0.6 + y_shift, l5 - 0.32, 'L5')
    ax0.text(0.6 + y_shift, l6 - 0.5, 'L6')

    ax0.set_xlabel(r'y $\rightarrow$')
    ax0.set_ylabel(r'x $\rightarrow$')
    ax0.plot([-0.46 + y_shift, -0.16 + y_shift], [-1.43 + x_shift, -1.43 + x_shift], 'r', lw=2, clip_on=False)
    ax0.text(-0.45 + y_shift, -1.37 + x_shift, '300 $\mu m$', clip_on=False, color='r')


def make_figure_14():

    folder = 'kCSD'
    filter_N = 5

    csd_data = np.load(join(folder, 'csd_data.npz'))
    csd_0_lfp_20 = csd_data['lfp20_csd0'][1:-1, 1:-1]
    csd_20_lfp_20 = csd_data['lfp20_csd20'][1:-1, 1:-1]
    csd_0_lfp_0 = csd_data['lfp0_csd0'][1:-1, 1:-1]

    ground_truth = np.load(join(folder, 'fig4truecsd_corrected.npy')).T / .3  # Divide by thickness
    ground_truth_smooth = smooth(ground_truth, filter_N)

    elec_y = np.arange(0.0, 1.1, 0.1)
    elec_x = np.arange(0.0, 3.1, 0.1)

    mc = 7
    csd_0_lfp_20 /= 1000.
    csd_20_lfp_20 /= 1000.
    csd_0_lfp_0 /= 1000.
    ground_truth /= 1000.
    ground_truth_smooth /= 1000.

    clr_map = plt.cm.bwr_r
    clevels = np.linspace(-1, 1, 16) * mc

    plot_c_kwargs = {'levels': clevels,
                    'linewidths': np.array([0.3]),
                    'extend': 'both',
                    'vmin': -1. * mc, 'vmax': 1. * mc,
                    'aspect': 'equal',
                    'extent': (min(elec_y), max(elec_y), min(elec_x), max(elec_x))}

    plot_c_diff_kwargs = {'levels': clevels / 10.,
                    'linewidths': np.array([0.3]),
                    'extend': 'both',
                    'vmin': -1. * mc / 10., 'vmax': 1. * mc / 10.,
                    'aspect': 'equal',
                    'extent': (min(elec_y), max(elec_y), min(elec_x), max(elec_x))}

    fig = plt.figure(figsize=[6., 5.])
    fig.subplots_adjust(left=0.05, right=0.9, wspace=0.4, bottom=0.01, hspace=0.2, top=0.94)

    ax_dict = {'xticks': [],
               'yticks': [],
               'aspect': 'equal',
               }
    ax1 = fig.add_subplot(241, xticks=[], yticks=[], aspect='equal', ylim=[0, 3], xlim=[0,1])
    ax2 = fig.add_subplot(242, title='Ground truth', **ax_dict)
    ax3 = fig.add_subplot(243, title='Smoothed\nground truth', **ax_dict)

    ax4 = fig.add_subplot(244, title='kCSD$_{20}$ on\n300 $\mu m$ thick slice', **ax_dict)
    ax5 = fig.add_subplot(245, title='Reconstructed by kCSD$_{0}$\non semi-infinite slice', **ax_dict)
    ax6 = fig.add_subplot(246, title='kCSD$_{0}$ on\n300 $\mu m$ thick slice', **ax_dict)
    ax7 = fig.add_subplot(247, title='CSD difference\nPanel D - Panel E', **ax_dict)
    ax8 = fig.add_subplot(248, title='CSD difference\nPanel D - Panel F', **ax_dict)

    draw_traub(ax1)
    ax2.axis(ax1.axis())

    # mark_subplots(ax0, xpos=0.1, ypos=1.1)
    mark_subplots([ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8], xpos=-0.17, ypos=1.0)
    # cax_3 = fig.add_axes([0.93, 0.2, 0.01, 0.6], xlabel='$\mu A/mm^3$')

    img2 = ax2.contourf(ground_truth, cmap=clr_map, **plot_c_kwargs)
    ax2.contour(ground_truth, linestyles='-', colors='k', **plot_c_kwargs)

    img3 = ax3.contourf(ground_truth_smooth, cmap=clr_map, **plot_c_kwargs)
    ax3.contour(ground_truth_smooth, linestyles='-', colors='k', **plot_c_kwargs)

    img4 = ax4.contourf(csd_20_lfp_20, cmap=clr_map, **plot_c_kwargs)
    ax4.contour(csd_20_lfp_20, linestyles='-', colors='k', **plot_c_kwargs)

    img5 = ax5.contourf(csd_0_lfp_0, cmap=clr_map, **plot_c_kwargs)
    ax5.contour(csd_0_lfp_0, linestyles='-', colors='k', **plot_c_kwargs)

    img6 = ax6.contourf(csd_0_lfp_20, cmap=clr_map, **plot_c_kwargs)
    ax6.contour(csd_0_lfp_20, linestyles='-', colors='k', **plot_c_kwargs)

    img7 = ax7.contourf(csd_20_lfp_20 - csd_0_lfp_0, cmap=clr_map, **plot_c_diff_kwargs)
    ax7.contour(csd_20_lfp_20 - csd_0_lfp_0, linestyles='-', colors='k', **plot_c_diff_kwargs)

    img8 = ax8.contourf(csd_20_lfp_20 - csd_0_lfp_20, cmap=clr_map, **plot_c_diff_kwargs)
    ax8.contour(csd_20_lfp_20 - csd_0_lfp_20, linestyles='-', colors='k', **plot_c_diff_kwargs)

    cbar_2 = plt.colorbar(img2, ax=ax2)
    cbar_2.set_ticks(clevels[::3])
    cbar_2.set_label('$\mu A/mm^3$')
    cbar_3 = plt.colorbar(img3, ax=ax3)
    cbar_3.set_ticks(clevels[::3])
    cbar_3.set_label('$\mu A/mm^3$')
    cbar_4 = plt.colorbar(img4, ax=ax4)
    cbar_4.set_ticks(clevels[::3])
    cbar_4.set_label('$\mu A/mm^3$')
    cbar_5 = plt.colorbar(img5, ax=ax5)
    cbar_5.set_ticks(clevels[::3])
    cbar_5.set_label('$\mu A/mm^3$')
    cbar_6 = plt.colorbar(img6, ax=ax6)
    cbar_6.set_ticks(clevels[::3])
    cbar_6.set_label('$\mu A/mm^3$')
    cbar_7 = plt.colorbar(img7, ax=ax7)
    cbar_7.set_ticks(clevels[::3] / 10.)
    cbar_7.set_label('$\mu A/mm^3$')
    cbar_8 = plt.colorbar(img8, ax=ax8)
    cbar_8.set_ticks(clevels[::3] / 10.)
    cbar_8.set_label('$\mu A/mm^3$')

    fig.savefig('figure_14.png', dpi=150)

if __name__ == '__main__':
    make_figure_14()
