All code needed to remake all figures from the paper "Modelling and Analysis of Electrical Potentials Recorded
in Microelectrode Arrays (MEAs)" by Ness et al. (2015), Neuroinformatics
  http://dx.doi.org/10.1007/s12021-015-9265-6

Any question can be directed to torbness@gmail.com