
import pylab as plt
from matplotlib import colors
from matplotlib.colors import LogNorm
import numpy as np


# THIS IS NOW MY PAPER FIGURE DEFAULT
plt.rcParams.update({'font.size': 5,
                    'figure.facecolor': '1',
                    'wspace': 0.5,
                    'hspace': 0.5,
                    'figure.subplot.wspace': 0.5,
                    'figure.subplot.hspace': 0.5,
                    'legend.fontsize': 7,
                    'axes.titlesize': 7,
                    'axes.labelsize': 7,
                    'xtick.labelsize': 7,
                    'ytick.labelsize': 7,
                    'font.family': 'arial',
                    })

colormap_LFP = [[1, 1, 0], [1, 0.96, 0], [1, 0.92, 0], [1, 0.88, 0], [1, 0.84, 0], [1, 0.80, 0],
                [1, 0.76, 0], [1, 0.72, 0], [1, 0.68, 0], [1, 0.64, 0], [1, 0.60, 0], [1, 0.56, 0],
                [1, 0.52, 0], [1, 0.48, 0], [1, 0.44, 0], [1, 0.40, 0], [1, 0.36, 0], [1, 0.32, 0],
                [1, 0.28, 0], [1, 0.24, 0], [1, 0.20, 0], [1, 0.16, 0], [1, 0.12, 0], [1, 0.08, 0],
                [1, 0.04, 0], [1, 0, 0], [0.96, 0, 0], [0.92, 0, 0], [0.88, 0, 0], [0.84, 0, 0],
                [0.80, 0, 0], [0.76, 0, 0], [0.72, 0, 0], [0.68, 0, 0], [0.64, 0, 0], [0.60, 0, 0],
                [0.56, 0, 0], [0.52, 0, 0], [0.48, 0, 0], [0.44, 0, 0], [0.40, 0, 0], [0.36, 0, 0],
                [0.32, 0, 0], [0.28, 0, 0], [0.24, 0, 0], [0.20, 0, 0], [0.16, 0, 0], [0.12, 0, 0],
                [0.08, 0, 0], [0.04, 0, 0], [0, 0, 0], [0, 0, 0.04], [0, 0, 0.08], [0, 0, 0.12],
                [0, 0, 0.16], [0, 0, 0.20], [0, 0, 0.24], [0, 0, 0.28], [0, 0, 0.32], [0, 0, 0.36],
                [0, 0, 0.40], [0, 0, 0.44], [0, 0, 0.48], [0, 0, 0.52], [0, 0, 0.56], [0, 0, 0.60],
                [0, 0, 0.64], [0, 0, 0.68], [0, 0, 0.72], [0, 0, 0.76], [0, 0, 0.80], [0, 0, 0.84],
                [0, 0, 0.88], [0, 0, 0.92], [0, 0, 0.96], [0, 0, 1], [0, 0.04, 1], [0, 0.08, 1],
                [0, 0.12, 1], [0, 0.16, 1], [0, 0.20, 1], [0, 0.24, 1], [0, 0.28, 1], [0, 0.32, 1],
                [0, 0.36, 1], [0, 0.40, 1], [0, 0.44, 1], [0, 0.48, 1], [0, 0.52, 1], [0, 0.56, 1],
                [0, 0.60, 1], [0, 0.64, 1], [0, 0.68, 1], [0, 0.72, 1], [0, 0.76, 1], [0, 0.80, 1],
                [0, 0.84, 1], [0, 0.88, 1], [0, 0.92, 1], [0, 0.96, 1], [0, 1, 1]]

LFP_half = colors.ListedColormap(plt.cm.PRGn(np.arange(256))[128:-1, :], name='LFP_cmap_half')
LFP_whole = colors.ListedColormap(plt.cm.PRGn(np.arange(256)), name='LFP_cmap_whole')

# LFP_cmap = colors.ListedColormap(colormap_LFP, name='LFP_cmap')

def mark_subplots(axes, letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ', xpos=-0.12, ypos=1.15):

    if not type(axes) is list:
        axes = [axes]

    for idx, ax in enumerate(axes):
        ax.text(xpos, ypos, letters[idx].capitalize(),
                horizontalalignment='center',
                verticalalignment='center',
                fontweight='demibold',
                fontsize=12,
                transform=ax.transAxes)

def simplify_axes(axes):

    if not type(axes) is list:
        axes = [axes]

    for ax in axes:
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
