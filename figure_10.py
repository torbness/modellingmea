from os.path import join
from pylab import *
from plotting_convention import *
from params import *


def make_figure_10():

    folder = 'point_source_results'
    use_heights_plane = ['zf', 'z0', 'z1', 'z2', 'z4', 'z9']
    use_heights = ['ze2', 'ze3', 'ze', 'zf', 'z0', 'z1', 'z2', 'z4', 'z9']

    height_clrs = lambda idx: plt.cm.jet(int(256./len(use_heights_plane) * idx))
    fig = figure(figsize=[6.4, 2])
    fig.subplots_adjust(top=0.85, bottom=0.2, left=0.07, right=0.94)
    center_idx = plane_y.shape[1]/2

    amp0 = np.load(join(folder, 'fem_control_zd_mea_plane.npy'))[center_idx, center_idx]
    h0 = charge_z_positions['zd'] + 150
    normalize = amp0
    normalize_b = 0

    amp0 /= normalize
    ax1 = fig.add_subplot(141, xlabel='Source height [$\mu m$]', yscale='log',
                       ylabel='Center potential', ylim=[1e-2, 1], xlim=[0, 300])
    ax2 = fig.add_subplot(142, xlabel='Distance in MEA plane [$\mu m$]',
                       ylabel='Potential', ylim=[0, 1], xlim=[0, 500])
    ax3 = fig.add_subplot(143, xlabel='Distance in MEA plane [$\mu m$]',
                       ylabel='Potential', ylim=[0, 1], xlim=[0, 500])
    ax4 = fig.add_subplot(144, xlabel='Distance in MEA plane [$\mu m$]',
                       ylabel='Relative difference', ylim=[0, 0.6], xlim=[0, 500])
    mark_subplots(fig.axes)
    simplify_axes(fig.axes)

    param_sets = [set_control, set_sl1, set_sl5]

    amp_vs_height_dict = {'control': [],
                          'b': [],
                          'c': [],
                          'sl3': [],
                          'dist': []}

    param_set_name_dict = {'b': 'Thickness: 10 $\mu m$',
                       'c': 'Thickness: 30 $\mu m$',
                       'sl3': 'Thickness: 20 $\mu m$',
                       'control': 'No saline layer'}

    for zidx, z in enumerate(use_heights_plane):
        co = np.load(join(folder, 'fem_control_%s_mea_plane.npy' % z))[center_idx:, center_idx]
        si = np.load(join(folder, 'fem_c_%s_mea_plane.npy' % z))[center_idx:, center_idx]

        if zidx == 0:
            normalize_b = np.max(co)

        co1 = co / normalize_b
        si1 = si / normalize_b

        co2 = co / np.max(co)
        si2 = si / np.max(si)
        ax2.plot(plane_y[center_idx:, center_idx], co1, '-', color=height_clrs(zidx), lw=1.5)
        ax2.plot(plane_y[center_idx:, center_idx], si1, '--', color=height_clrs(zidx), lw=1.5)
        ax3.plot(plane_y[center_idx:, center_idx], co2, '-', color=height_clrs(zidx), lw=1.5)
        ax3.plot(plane_y[center_idx:, center_idx], si2, '--', color=height_clrs(zidx), lw=1.5)

        decrease = np.abs((si1 - co1) / co1)
        ax4.plot(plane_y[center_idx:, center_idx], decrease, label='z = %d $\mu m$' % (charge_z_positions[z] + 150),
                 color=height_clrs(zidx), lw=1.5)
        ax4.legend(frameon=False, bbox_to_anchor=[1.3, 1.1])


    for zidx, z in enumerate(use_heights):
        amp_vs_height_dict['dist'].append(charge_z_positions[z] + 150)
        for param_set in param_sets:
            name = param_set['set_name']
            try:
                amp = np.load(join(folder, 'fem_%s_%s_mea_plane.npy' % (name, z)))[center_idx, center_idx] / normalize
            except IOError:
                amp = np.nan
            if name == 'c' and charge_z_positions[z] < -119:
                amp = np.nan
            amp_vs_height_dict[name].append(amp)
        if zidx == 0:
            ax2.plot(0, 0, '-', color='k', label='No saline layer', lw=1.5)
            ax2.plot(0, 0, '--', color='k', label='Thickness: 30 $\mu m$', lw=1.5)
            ax2.legend(frameon=False, handlelength=3, loc=2)

    param_set_lines = []
    param_set_names = []

    for param_set in param_sets:
        if param_set['set_name'] == 'control':
            amps = np.r_[amp0, amp_vs_height_dict[param_set['set_name']]]
            dists = np.r_[h0, amp_vs_height_dict['dist']]
            l, = ax1.plot(dists, amps, c=param_set['clr'], lw=1.5)
        else:
            l, = ax1.plot(amp_vs_height_dict['dist'], amp_vs_height_dict[param_set['set_name']], c=param_set['clr'], lw=1.5)
        param_set_lines.append(l)
        param_set_names.append(param_set_name_dict[param_set['set_name']])
    ax1.legend(param_set_lines, param_set_names, frameon=False, bbox_to_anchor=[1.25, 1.2])
    plt.savefig('figure_10.png', dpi=150)

if __name__ == '__main__':
    make_figure_10()