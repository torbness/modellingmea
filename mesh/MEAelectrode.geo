
// Electrode 1 (Inner electrode)

pE10 = newp; Point(pE10) = {0, 0, -retina_thickness/2, meshElectrode};
pE11 = newp; Point(pE11) = {-elec_r1, 0, -retina_thickness/2, meshElectrode};
pE12 = newp; Point(pE12) = {0, elec_r1, -retina_thickness/2, meshElectrode};
pE13 = newp; Point(pE13) = {+elec_r1, 0, -retina_thickness/2, meshElectrode};
pE14 = newp; Point(pE14) = {0, -elec_r1, -retina_thickness/2, meshElectrode};

pE15 = newp; Point(pE15) = {0, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE16 = newp; Point(pE16) = {-elec_r1, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE17 = newp; Point(pE17) = {0, elec_r1, -retina_thickness/2-elec_depth, meshElectrode};
pE18 = newp; Point(pE18) = {+elec_r1, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE19 = newp; Point(pE19) = {0, -elec_r1, -retina_thickness/2-elec_depth, meshElectrode};

// Electrode surface lines
lE10 = newl; Circle(lE10) = {pE11, pE10, pE12};
lE11 = newl; Circle(lE11) = {pE12, pE10, pE13};
lE12 = newl; Circle(lE12) = {pE13, pE10, pE14};
lE13 = newl; Circle(lE13) = {pE14, pE10, pE11};

// Electrode bottom lines
lE14 = newl; Circle(lE14) = {pE16, pE15, pE17};
lE15 = newl; Circle(lE15) = {pE17, pE15, pE18};
lE16 = newl; Circle(lE16) = {pE18, pE15, pE19};
lE17 = newl; Circle(lE17) = {pE19, pE15, pE16};

// Electrode vertical lines
lE18 = newl; Line(lE18) = {pE11, pE16};
lE19 = newl; Line(lE19) = {pE12, pE17};
lE110 = newl; Line(lE110) = {pE13, pE18};
lE111 = newl; Line(lE111) = {pE14, pE19};

llE1Upper = newll; Line Loop(llE1Upper) = {lE10,lE11,lE12,lE13};
llE1Lower = newll; Line Loop(llE1Lower) = {lE14,lE15,lE16,lE17};

llE1Vert1 = newll; Line Loop(llE1Vert1) = {lE10, lE19, -lE14, -lE18};
sE1Vert1 = news; Ruled Surface(sE1Vert1) =  {llE1Vert1};
llE1Vert2 = newll; Line Loop(llE1Vert2) = {lE11, lE110, -lE15, -lE19};
sE1Vert2 = news; Ruled Surface(sE1Vert2) =  {llE1Vert2};
llE1Vert3 = newll; Line Loop(llE1Vert3) = {lE12, lE111, -lE16, -lE110};
sE1Vert3 = news; Ruled Surface(sE1Vert3) =  {llE1Vert3};
llE1Vert4 = newll; Line Loop(llE1Vert4) = {lE13, lE18, -lE17, -lE111};
sE1Vert4 = news; Ruled Surface(sE1Vert4) =  {llE1Vert4};

sE1Upper = news; Ruled Surface(sE1Upper) = {llE1Upper};
sE1Lower = news; Ruled Surface(sE1Lower) = {llE1Lower};

// Electrode 2 (Middle electrode)

pE20 = newp; Point(pE20) = {0, 0, -retina_thickness/2, meshElectrode};
pE21 = newp; Point(pE21) = {-elec_r2, 0, -retina_thickness/2, meshElectrode};
pE22 = newp; Point(pE22) = {0, elec_r2, -retina_thickness/2, meshElectrode};
pE23 = newp; Point(pE23) = {+elec_r2, 0, -retina_thickness/2, meshElectrode};
pE24 = newp; Point(pE24) = {0, -elec_r2, -retina_thickness/2, meshElectrode};

pE25 = newp; Point(pE25) = {0, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE26 = newp; Point(pE26) = {-elec_r2, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE27 = newp; Point(pE27) = {0, elec_r2, -retina_thickness/2-elec_depth, meshElectrode};
pE28 = newp; Point(pE28) = {+elec_r2, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE29 = newp; Point(pE29) = {0, -elec_r2, -retina_thickness/2-elec_depth, meshElectrode};

// Electrode surface lines
lE20 = newl; Circle(lE20) = {pE21, pE20, pE22};
lE21 = newl; Circle(lE21) = {pE22, pE20, pE23};
lE22 = newl; Circle(lE22) = {pE23, pE20, pE24};
lE23 = newl; Circle(lE23) = {pE24, pE20, pE21};

// Electrode bottom lines
lE24 = newl; Circle(lE24) = {pE26, pE25, pE27};
lE25 = newl; Circle(lE25) = {pE27, pE25, pE28};
lE26 = newl; Circle(lE26) = {pE28, pE25, pE29};
lE27 = newl; Circle(lE27) = {pE29, pE25, pE26};

// Electrode vertical lines
lE28 = newl; Line(lE28) = {pE21, pE26};
lE29 = newl; Line(lE29) = {pE22, pE27};
lE210 = newl; Line(lE210) = {pE23, pE28};
lE211 = newl; Line(lE211) = {pE24, pE29};

llE2Upper = newll; Line Loop(llE2Upper) = {lE20,lE21,lE22,lE23};
llE2Lower = newll; Line Loop(llE2Lower) = {lE24,lE25,lE26,lE27};

llE2Vert1 = newll; Line Loop(llE2Vert1) = {lE20, lE29, -lE24, -lE28};
sE2Vert1 = news; Ruled Surface(sE2Vert1) =  {llE2Vert1};
llE2Vert2 = newll; Line Loop(llE2Vert2) = {lE21, lE210, -lE25, -lE29};
sE2Vert2 = news; Ruled Surface(sE2Vert2) =  {llE2Vert2};
llE2Vert3 = newll; Line Loop(llE2Vert3) = {lE22, lE211, -lE26, -lE210};
sE2Vert3 = news; Ruled Surface(sE2Vert3) =  {llE2Vert3};
llE2Vert4 = newll; Line Loop(llE2Vert4) = {lE23, lE28, -lE27, -lE211};
sE2Vert4 = news; Ruled Surface(sE2Vert4) =  {llE2Vert4};

sE2Upper = news; Ruled Surface(sE2Upper) = {llE2Upper, llE1Upper};
sE2Lower = news; Ruled Surface(sE2Lower) = {llE2Lower, llE1Lower};

// Electrode 3 (Outer electrode)

pE30 = newp; Point(pE30) = {0, 0, -retina_thickness/2, meshElectrode};
pE31 = newp; Point(pE31) = {-elec_r3, 0, -retina_thickness/2, meshElectrode};
pE32 = newp; Point(pE32) = {0, elec_r3, -retina_thickness/2, meshElectrode};
pE33 = newp; Point(pE33) = {+elec_r3, 0, -retina_thickness/2, meshElectrode};
pE34 = newp; Point(pE34) = {0, -elec_r3, -retina_thickness/2, meshElectrode};

pE35 = newp; Point(pE35) = {0, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE36 = newp; Point(pE36) = {-elec_r3, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE37 = newp; Point(pE37) = {0, elec_r3, -retina_thickness/2-elec_depth, meshElectrode};
pE38 = newp; Point(pE38) = {+elec_r3, 0, -retina_thickness/2-elec_depth, meshElectrode};
pE39 = newp; Point(pE39) = {0, -elec_r3, -retina_thickness/2-elec_depth, meshElectrode};

// Electrode surface lines
lE30 = newl; Circle(lE30) = {pE31, pE30, pE32};
lE31 = newl; Circle(lE31) = {pE32, pE30, pE33};
lE32 = newl; Circle(lE32) = {pE33, pE30, pE34};
lE33 = newl; Circle(lE33) = {pE34, pE30, pE31};

// Electrode bottom lines
lE34 = newl; Circle(lE34) = {pE36, pE35, pE37};
lE35 = newl; Circle(lE35) = {pE37, pE35, pE38};
lE36 = newl; Circle(lE36) = {pE38, pE35, pE39};
lE37 = newl; Circle(lE37) = {pE39, pE35, pE36};

// Electrode vertical lines
lE38 = newl; Line(lE38) = {pE31, pE36};
lE39 = newl; Line(lE39) = {pE32, pE37};
lE310 = newl; Line(lE310) = {pE33, pE38};
lE311 = newl; Line(lE311) = {pE34, pE39};

llE3Upper = newll; Line Loop(llE3Upper) = {lE30,lE31,lE32,lE33};
llE3Lower = newll; Line Loop(llE3Lower) = {lE34,lE35,lE36,lE37};

llE3Vert1 = newll; Line Loop(llE3Vert1) = {lE30, lE39, -lE34, -lE38};
sE3Vert1 = news; Ruled Surface(sE3Vert1) =  {llE3Vert1};
llE3Vert2 = newll; Line Loop(llE3Vert2) = {lE31, lE310, -lE35, -lE39};
sE3Vert2 = news; Ruled Surface(sE3Vert2) =  {llE3Vert2};
llE3Vert3 = newll; Line Loop(llE3Vert3) = {lE32, lE311, -lE36, -lE310};
sE3Vert3 = news; Ruled Surface(sE3Vert3) =  {llE3Vert3};
llE3Vert4 = newll; Line Loop(llE3Vert4) = {lE33, lE38, -lE37, -lE311};
sE3Vert4 = news; Ruled Surface(sE3Vert4) =  {llE3Vert4};

sE3Upper = news; Ruled Surface(sE3Upper) = {llE3Upper, llE2Upper};
sE3Lower = news; Ruled Surface(sE3Lower) = {llE3Lower, llE2Lower};
