
meshElectrode = 1.;
meshInnerRetina = 2.5;
meshRetina = 500;
meshSaline = 1500;

cylinder_radius = 8000;
cylinder_height = cylinder_radius;

retina_thickness = 300; // [um]
retina_radius = cylinder_radius;
inner_retina_radius = 20;
inner_retina_thickness = retina_thickness;

elec_r1 = 5;
elec_r2 = 10;
elec_r3 = 15;

elec_depth = 10.;
