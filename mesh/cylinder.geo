//CYLINDER
pCyl5 = newp; Point(pCyl5) = {cylinder_radius, 0, +retina_thickness/2 +cylinder_height, meshSaline};
pCyl6 = newp; Point(pCyl6) = {0, 0, +retina_thickness/2 +cylinder_height, meshSaline};
pCyl7 = newp; Point(pCyl7) = {0, cylinder_radius, +retina_thickness/2 +cylinder_height, meshSaline};
pCyl8 = newp; Point(pCyl8) = {-cylinder_radius, 0, +retina_thickness/2 +cylinder_height, meshSaline};
pCyl9 = newp; Point(pCyl9) = {0, -cylinder_radius, +retina_thickness/2 +cylinder_height, meshSaline};

lCyl5 = newl; Circle(lCyl5) = {pCyl5, pCyl6, pCyl7};
lCyl6 = newl; Circle(lCyl6) = {pCyl7, pCyl6, pCyl8};
lCyl7 = newl; Circle(lCyl7) = {pCyl8, pCyl6, pCyl9};
lCyl8 = newl; Circle(lCyl8) = {pCyl9, pCyl6, pCyl5};

lCyl9 = newl; Line(lCyl9) = {p5, pCyl5};
lCyl10 = newl; Line(lCyl10) = {p7, pCyl7};
lCyl11 = newl; Line(lCyl11) = {p8, pCyl8};
lCyl12 = newl; Line(lCyl12) = {p9, pCyl9};

cylinderLineLoop1  = newll; Line Loop(cylinderLineLoop1) = {lCyl7, -lCyl12, -l7, lCyl11};
cylinderSurf1 = news; Ruled Surface(cylinderSurf1) = {cylinderLineLoop1};
cylinderLineLoop2  = newll; Line Loop(cylinderLineLoop2) = {lCyl11, -lCyl6, -lCyl10, l6};
cylinderSurf2 = news; Ruled Surface(cylinderSurf2) = {cylinderLineLoop2};
cylinderLineLoop3  = newll; Line Loop(cylinderLineLoop3) = {lCyl10, -lCyl5, -lCyl9, l5};
cylinderSurf3 = news; Ruled Surface(cylinderSurf3) = {cylinderLineLoop3};
cylinderLineLoop4  = newll; Line Loop(cylinderLineLoop4) = {lCyl9, -lCyl8, -lCyl12, l8};
cylinderSurf4 = news; Ruled Surface(cylinderSurf4) = {cylinderLineLoop4};
uCylinderLineLoop = newll; Line Loop(uCylinderLineLoop) = {lCyl8, lCyl5, lCyl6, lCyl7};
uCylinderSurf = news; Ruled Surface(uCylinderSurf) = {uCylinderLineLoop};
