// InnerRetina
pInner0 = newp; Point(pInner0) = {0, 0, -inner_retina_thickness/2, meshInnerRetina};
pInner1 = newp; Point(pInner1) = {inner_retina_radius, 0, -inner_retina_thickness/2, meshInnerRetina};
pInner2 = newp; Point(pInner2) = {0, inner_retina_radius, -inner_retina_thickness/2, meshInnerRetina};
pInner3 = newp; Point(pInner3) = {-inner_retina_radius, 0, -inner_retina_thickness/2, meshInnerRetina};
pInner4 = newp; Point(pInner4) = {0, -inner_retina_radius, -inner_retina_thickness/2, meshInnerRetina};
pInner5 = newp; Point(pInner5) = {inner_retina_radius, 0, +inner_retina_thickness/2, meshInnerRetina};
pInner6 = newp; Point(pInner6) = {0, 0, +inner_retina_thickness/2, meshInnerRetina};
pInner7 = newp; Point(pInner7) = {0, inner_retina_radius, +inner_retina_thickness/2, meshInnerRetina};
pInner8 = newp; Point(pInner8) = {-inner_retina_radius, 0, +inner_retina_thickness/2, meshInnerRetina};
pInner9 = newp; Point(pInner9) = {0, -inner_retina_radius, +inner_retina_thickness/2, meshInnerRetina};

p0 = newp; Point(p0) = {0, 0, -retina_thickness/2, meshRetina};
p1 = newp; Point(p1) = {cylinder_radius, 0, -retina_thickness/2, meshRetina};
p2 = newp; Point(p2) = {0, cylinder_radius, -retina_thickness/2, meshRetina};
p3 = newp; Point(p3) = {-cylinder_radius, 0, -retina_thickness/2, meshRetina};
p4 = newp; Point(p4) = {0, -cylinder_radius, -retina_thickness/2, meshRetina};
p5 = newp; Point(p5) = {cylinder_radius, 0, +retina_thickness/2, meshRetina};
p6 = newp; Point(p6) = {0, 0, +retina_thickness/2, meshRetina};
p7 = newp; Point(p7) = {0, cylinder_radius, +retina_thickness/2, meshRetina};
p8 = newp; Point(p8) = {-cylinder_radius, 0, +retina_thickness/2, meshRetina};
p9 = newp; Point(p9) = {0, -cylinder_radius, +retina_thickness/2, meshRetina};

lInner1 = newl; Circle(lInner1) = {pInner1, pInner0, pInner2};
lInner2 = newl; Circle(lInner2) = {pInner2, pInner0, pInner3};
lInner3 = newl; Circle(lInner3) = {pInner3, pInner0, pInner4};
lInner4 = newl; Circle(lInner4) = {pInner4, pInner0, pInner1};
lInner5 = newl; Circle(lInner5) = {pInner5, pInner6, pInner7};
lInner6 = newl; Circle(lInner6) = {pInner7, pInner6, pInner8};
lInner7 = newl; Circle(lInner7) = {pInner8, pInner6, pInner9};
lInner8 = newl; Circle(lInner8) = {pInner9, pInner6, pInner5};
lInner9 = newl; Line(lInner9) = {pInner1, pInner5};
lInner10 = newl; Line(lInner10) = {pInner2, pInner7};
lInner11 = newl; Line(lInner11) = {pInner3, pInner8};
lInner12 = newl; Line(lInner12) = {pInner4, pInner9}; 

l1 = newl; Circle(l1) = {p1, p0, p2};
l2 = newl; Circle(l2) = {p2, p0, p3};
l3 = newl; Circle(l3) = {p3, p0, p4};
l4 = newl; Circle(l4) = {p4, p0, p1};
l5 = newl; Circle(l5) = {p5, p6, p7};
l6 = newl; Circle(l6) = {p7, p6, p8};
l7 = newl; Circle(l7) = {p8, p6, p9};
l8 = newl; Circle(l8) = {p9, p6, p5};
l9 = newl; Line(l9) = {p1, p5};
l10 = newl; Line(l10) = {p2, p7};
l11 = newl; Line(l11) = {p3, p8};
l12 = newl; Line(l12) = {p4, p9}; 

lo_ret_ll = newll;Line Loop(lo_ret_ll) = {l1,l2,l3,l4};
up_ret_ll = newll; Line Loop(up_ret_ll) = {l5,l6,l7,l8};
//OUTER  VERTICAL SURFACES
vert_ret_ll1 = newll; Line Loop(vert_ret_ll1) = {l1,l10,-l5,-l9};
vert_ret_surf1 = news; Ruled Surface(vert_ret_surf1) = {vert_ret_ll1};

vert_ret_ll2 = newll; Line Loop(vert_ret_ll2) = {l2,l11,-l6,-l10};
vert_ret_surf2 = news; Ruled Surface(vert_ret_surf2) = {vert_ret_ll2};

vert_ret_ll3 = newll; Line Loop(vert_ret_ll3) = {l3,l12,-l7,-l11};
vert_ret_surf3 = news; Ruled Surface(vert_ret_surf3) = {vert_ret_ll3};

vert_ret_ll4 = newll; Line Loop(vert_ret_ll4) = {l4,l9,-l8,-l12};
vert_ret_surf4 = news; Ruled Surface(vert_ret_surf4) = {vert_ret_ll4};

//INNER VERTICAL SURFACES
lo_inner_ret_ll = newll;Line Loop(lo_inner_ret_ll) = {lInner1,lInner2,lInner3,lInner4};
lo_inner_ret_surf = news; Ruled Surface(lo_inner_ret_surf) = {lo_inner_ret_ll, llE3Upper};

up_inner_ret_ll = newll;Line Loop(up_inner_ret_ll) = {lInner5,lInner6,lInner7,lInner8};
up_inner_ret_surf = news; Ruled Surface(up_inner_ret_surf) = {up_inner_ret_ll};

vert_inner_ret_ll1 = newll; Line Loop(vert_inner_ret_ll1) = {lInner1,lInner10,-lInner5,-lInner9};
vert_inner_ret_surf1 = news; Ruled Surface(vert_inner_ret_surf1) = {vert_inner_ret_ll1};

vert_inner_ret_ll2 = newll; Line Loop(vert_inner_ret_ll2) = {lInner2,lInner11,-lInner6,-lInner10};
vert_inner_ret_surf2 = news; Ruled Surface(vert_inner_ret_surf2) = {vert_inner_ret_ll2};

vert_inner_ret_ll3 = newll; Line Loop(vert_inner_ret_ll3) = {lInner3,lInner12,-lInner7,-lInner11};
vert_inner_ret_surf3 = news; Ruled Surface(vert_inner_ret_surf3) = {vert_inner_ret_ll3};

vert_inner_ret_ll4 = newll; Line Loop(vert_inner_ret_ll4) = {lInner4,lInner9,-lInner8,-lInner12};
vert_inner_ret_surf4 = news; Ruled Surface(vert_inner_ret_surf4) = {vert_inner_ret_ll4};

up_ret_surf = news; Ruled Surface(up_ret_surf) = {up_ret_ll, up_inner_ret_ll};
lo_ret_surf = news; Ruled Surface(lo_ret_surf) = {lo_ret_ll, lo_inner_ret_ll};
