
// Electrode 1 (Inner electrode)

pE10 = newp; Point(pE10) = {0, 0, -retina_thickness/2, meshElectrode};
pE11 = newp; Point(pE11) = {-elec_r1, 0, -retina_thickness/2, meshElectrode};
pE12 = newp; Point(pE12) = {0, elec_r1, -retina_thickness/2, meshElectrode};
pE13 = newp; Point(pE13) = {+elec_r1, 0, -retina_thickness/2, meshElectrode};
pE14 = newp; Point(pE14) = {0, -elec_r1, -retina_thickness/2, meshElectrode};

// Electrode surface lines
lE10 = newl; Circle(lE10) = {pE11, pE10, pE12};
lE11 = newl; Circle(lE11) = {pE12, pE10, pE13};
lE12 = newl; Circle(lE12) = {pE13, pE10, pE14};
lE13 = newl; Circle(lE13) = {pE14, pE10, pE11};

llE1Upper = newll; Line Loop(llE1Upper) = {lE10,lE11,lE12,lE13};
sE1Upper = news; Ruled Surface(sE1Upper) = {llE1Upper};
// Electrode 2 (Middle electrode)

pE20 = newp; Point(pE20) = {0, 0, -retina_thickness/2, meshElectrode};
pE21 = newp; Point(pE21) = {-elec_r2, 0, -retina_thickness/2, meshElectrode};
pE22 = newp; Point(pE22) = {0, elec_r2, -retina_thickness/2, meshElectrode};
pE23 = newp; Point(pE23) = {+elec_r2, 0, -retina_thickness/2, meshElectrode};
pE24 = newp; Point(pE24) = {0, -elec_r2, -retina_thickness/2, meshElectrode};

// Electrode surface lines
lE20 = newl; Circle(lE20) = {pE21, pE20, pE22};
lE21 = newl; Circle(lE21) = {pE22, pE20, pE23};
lE22 = newl; Circle(lE22) = {pE23, pE20, pE24};
lE23 = newl; Circle(lE23) = {pE24, pE20, pE21};

llE2Upper = newll; Line Loop(llE2Upper) = {lE20,lE21,lE22,lE23};
sE2Upper = news; Ruled Surface(sE2Upper) = {llE2Upper, llE1Upper};

// Electrode 3 (Outer electrode)

pE30 = newp; Point(pE30) = {0, 0, -retina_thickness/2, meshElectrode};
pE31 = newp; Point(pE31) = {-elec_r3, 0, -retina_thickness/2, meshElectrode};
pE32 = newp; Point(pE32) = {0, elec_r3, -retina_thickness/2, meshElectrode};
pE33 = newp; Point(pE33) = {+elec_r3, 0, -retina_thickness/2, meshElectrode};
pE34 = newp; Point(pE34) = {0, -elec_r3, -retina_thickness/2, meshElectrode};

// Electrode surface lines
lE30 = newl; Circle(lE30) = {pE31, pE30, pE32};
lE31 = newl; Circle(lE31) = {pE32, pE30, pE33};
lE32 = newl; Circle(lE32) = {pE33, pE30, pE34};
lE33 = newl; Circle(lE33) = {pE34, pE30, pE31};

llE3Upper = newll; Line Loop(llE3Upper) = {lE30,lE31,lE32,lE33};
sE3Upper = news; Ruled Surface(sE3Upper) = {llE3Upper, llE2Upper};

