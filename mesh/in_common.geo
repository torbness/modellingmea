
electrodeSurfaces[] = {};
electrodeLineLoops[] = {};
electrodeVolumes[] = {};

Include 'MEAelectrode.geo';
Include 'innerRetina.geo';
Include 'cylinder.geo';

salineSurfaceLoop = newsl; Surface Loop(salineSurfaceLoop) = {cylinderSurf1,cylinderSurf2,
cylinderSurf3,cylinderSurf4,uCylinderSurf, up_ret_surf, up_inner_ret_surf};
salineVolume = newv; Volume(salineVolume) = {salineSurfaceLoop};

outerRetinaSurfaceLoop = newsl; Surface Loop(outerRetinaSurfaceLoop) = {vert_ret_surf1, vert_ret_surf2, 
		       vert_ret_surf3, vert_ret_surf4, up_ret_surf, lo_ret_surf,
		       vert_inner_ret_surf1, vert_inner_ret_surf2, vert_inner_ret_surf3, vert_inner_ret_surf4};
outerRetinaVolume = newv; Volume(outerRetinaVolume) = {outerRetinaSurfaceLoop};
innerRetinaSurfaceLoop = newsl; Surface Loop(innerRetinaSurfaceLoop) = {vert_inner_ret_surf1, 
		       vert_inner_ret_surf2, vert_inner_ret_surf3, vert_inner_ret_surf4,
		       lo_inner_ret_surf, up_inner_ret_surf, sE3Upper, sE2Upper, sE1Upper};
innerRetinaVolume = newv; Volume(innerRetinaVolume) = {innerRetinaSurfaceLoop};


