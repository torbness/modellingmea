Include "params.geo";
Include 'in_common.geo';
Include 'E3.geo';
Physical Volume(1) = {salineVolume, innerRetinaVolume, outerRetinaVolume, vE1, vE2, vE3};
Physical Surface(9) = {cylinderSurf1,cylinderSurf2,cylinderSurf3,cylinderSurf4,
	 	       uCylinderSurf, vert_ret_surf1, vert_ret_surf2, 
		       vert_ret_surf3, vert_ret_surf4};