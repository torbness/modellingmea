import numpy as np
import os
from os.path import join
import sys
import pylab as plt
from plotting_convention import *
import ViMEAPy

from params import *

plt.seed(1234)

def moi_calculations():
    """Do MoI calculations"""
    
    out_folder = 'point_source_results'

    elec_x = plane_x.flatten()
    elec_y = plane_y.flatten()
    moi_kwargs = {'elec_x': elec_x,
                  'elec_y': elec_y,
                  'elec_z': elec_z,
                  'steps': 20,
                  'xmid': np.array([0.0]),
                  'ymid': np.array([0.0]),
                  }

    moi_kwargs_ground = {'elec_x': np.array([mesh_dict['cylinder_radius']]),
                        'elec_y': np.array([0.]),
                        'elec_z': elec_z,
                        'steps': 20,
                        'xmid': np.array([0.0]),
                        'ymid': np.array([0.0]),
                  }
    ground_pos = np.array([mesh_dict['cylinder_radius'], 0, elec_z])
    for zpos_name in charge_z_positions:
        print "Zpos: ", zpos_name
        charge_pos = np.array([0, 0, charge_z_positions[zpos_name]])
        
        control = ViMEAPy.PS_without_elec_mapping_shifted(sigma_T=set_control['sigma_T'],
                                              sigma_S=set_control['sigma_S'],
                                              zmid=np.array([charge_z_positions[zpos_name]]),
                                              **moi_kwargs)
        no_saline = ViMEAPy.PS_without_elec_mapping_shifted(sigma_T=set_a['sigma_T'],
                                                sigma_S=set_a['sigma_S'],
                                                zmid=np.array([charge_z_positions[zpos_name]]),
                                                **moi_kwargs)
        anis_d = ViMEAPy.anisotropic_PS_without_elec_mapping_shifted(sigma_T=set_d['sigma_T'],
                                                         sigma_S=np.array([set_d['sigma_S']]*3),
                                                         zmid=np.array([charge_z_positions[zpos_name]]),
                                                         **moi_kwargs)
        anis_e = ViMEAPy.anisotropic_PS_without_elec_mapping_shifted(sigma_T=set_e['sigma_T'],
                                                         sigma_S=np.array([set_e['sigma_S']]*3),
                                                         zmid=np.array([charge_z_positions[zpos_name]]),
                                                         **moi_kwargs)

        anis_anis2 = ViMEAPy.anisotropic_PS_without_elec_mapping_shifted(sigma_T=set_anis2['sigma_T'],
                                                         sigma_S=np.array([set_anis2['sigma_S']]*3),
                                                         zmid=np.array([charge_z_positions[zpos_name]]),
                                                         **moi_kwargs)

        # control_ground = MoI.PS_without_elec_mapping(sigma_T=set_control['sigma_T'],
        #                                       sigma_S=set_control['sigma_S'],
        #                                       zmid=np.array([charge_z_positions[zpos_name]]),
        #                                       **moi_kwargs_ground)[0, 0]

        control_ground = 0
        no_saline_ground = 0
        anis_d_ground = 0
        anis_e_ground = 0
        anis_anis2_ground = 0

        np.save(join(out_folder, 'moi_a_%s_mea_plane.npy' % zpos_name),
                no_saline.reshape(plane_x.shape) - no_saline_ground)
        np.save(join(out_folder, 'moi_control_%s_mea_plane.npy' % zpos_name),
                control.reshape(plane_x.shape) - control_ground)
        np.save(join(out_folder, 'moi_d_%s_mea_plane.npy' % zpos_name),
                anis_d.reshape(plane_x.shape) - anis_d_ground)
        np.save(join(out_folder, 'moi_e_%s_mea_plane.npy' % zpos_name),
                anis_e.reshape(plane_x.shape) - anis_e_ground)
        np.save(join(out_folder, 'moi_anis2_%s_mea_plane.npy' % zpos_name),
                anis_anis2.reshape(plane_x.shape) - anis_anis2_ground)

        mea_plane = np.zeros(plane_y.shape)
        for col in xrange(plane_x.shape[1]):
            for row in xrange(plane_x.shape[0]):
                elec_pos = np.array([plane_x[row, col], plane_y[row, col], -slice_thickness/2.])
                mea_plane[row, col] = 1 / (4 * np.pi * 0.3) * 1 / np.sqrt(np.sum((elec_pos - charge_pos)**2))
        mea_plane -= 0  #1 / (4 * np.pi * 0.3) * 1 / np.sqrt(np.sum((ground_pos - charge_pos)**2))
        np.save(join(out_folder, 'moi_i_%s_mea_plane.npy' % zpos_name), mea_plane)
        crossection = np.zeros(crossection_x.shape)
        for col in xrange(crossection_x.shape[1]):
            for row in xrange(crossection_x.shape[0]):
                elec_pos = np.array([crossection_x[row, col], 10, crossection_z[row, col]])
                crossection[row, col] = 1 / (4 * np.pi * 0.3) * 1 / np.sqrt(np.sum((elec_pos - charge_pos)**2))
                
        crossection -= 0  #1 / (4 * np.pi * 0.3) * 1 / np.sqrt(np.sum((ground_pos - charge_pos)**2))
        np.save(join(out_folder, 'moi_i_%s_crossection.npy' % zpos_name), crossection)


if __name__ == '__main__':

    moi_calculations()