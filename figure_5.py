import numpy as np
import os
from os.path import join
import sys
import dolfin as df
import pylab as plt
import ViMEAPy
from plotting_convention import *
import ConductivityClass
from params import *


plt.seed(1234)

def electrode_fem(solver_params, charges_pos, elec_pos,
                                magnitudes, mesh_dict, elec_name):
    """ The potential from charges in the electrode is found by FEM"""

    print "Loading meshes ..."    
    mesh = df.Mesh(join(mesh_dict['folder'], "mesh_%s.xml" % elec_name))
    subdomains = df.MeshFunction("size_t", mesh,
                              join(mesh_dict['folder'], "mesh_%s_physical_region.xml" % elec_name))
    boundaries = df.MeshFunction("size_t", mesh,
                              join(mesh_dict['folder'], "mesh_%s_facet_region.xml" % elec_name))
    V = df.FunctionSpace(mesh, "CG", solver_params["LagrangeP"])
    Vs = df.FunctionSpace(mesh, "DG", 0)
    values = np.zeros(len(charges_pos))
    readout_pos = elec_pos[0], elec_pos[1], elec_pos[2] - mesh_dict['elec_depth']/2.
    sigma_class = ConductivityClass.ElectrodeConductivity(**set_electrode)
    sigma = sigma_class.return_conductivity_tensor(Vs)

    for idx, pos in enumerate(charges_pos):
        print "\033[1;35mSimulation: %d of %d\033[1;m" % (idx + 1, len(charges_pos))
        v = df.TestFunction(V)
        u = df.TrialFunction(V)
        a = df.inner(sigma * df.grad(u), df.grad(v))*df.dx(1)
        L = df.Constant(0)*v*df.dx
        A = df.assemble(a, cell_domains=subdomains, exterior_facet_domains=boundaries)
        b = df.assemble(L, cell_domains=subdomains, exterior_facet_domains=boundaries)
        bc = df.DirichletBC(V, df.Constant(0), boundaries, 9)
        bc.apply(A, b)
        point = df.Point(pos[0], pos[1], pos[2])
        delta = df.PointSource(V, point, magnitudes[idx])
        delta.apply(b)
        phi = df.Function(V)
        df.solve(A, phi.vector(), b, solver_params["linear_solver"],
              solver_params["preconditioner"])
        values[idx] = phi(readout_pos)
    return values

def electrode_fem_recip(solver_params, charges_pos, elec_pos,
                                magnitudes, mesh_dict, elec_name):
    """ The potential from charges in the electrode is found by FEM"""

    print "Loading meshes ..."
    mesh = df.Mesh(join(mesh_dict['folder'], "mesh_%s.xml" % elec_name))
    subdomains = df.MeshFunction("size_t", mesh,
                                 join(mesh_dict['folder'], "mesh_%s_physical_region.xml" % elec_name))
    boundaries = df.MeshFunction("size_t", mesh,
                                 join(mesh_dict['folder'], "mesh_%s_facet_region.xml" % elec_name))
    V = df.FunctionSpace(mesh, "CG", solver_params["LagrangeP"])
    Vs = df.FunctionSpace(mesh, "DG", 0)
    values = np.zeros(len(charges_pos))
    readout_pos = elec_pos[0], elec_pos[1], elec_pos[2] - mesh_dict['elec_depth']/2.
    print "Readout pos ", readout_pos
    sigma_class = ConductivityClass.ElectrodeConductivity(**set_electrode)
    sigma = sigma_class.return_conductivity_tensor(Vs)

    v = df.TestFunction(V)
    u = df.TrialFunction(V)
    a = df.inner(sigma * df.grad(u), df.grad(v)) * df.dx(1)
    L = df.Constant(0) * v * df.dx
    A = df.assemble(a, cell_domains=subdomains, exterior_facet_domains=boundaries)
    b = df.assemble(L, cell_domains=subdomains, exterior_facet_domains=boundaries)
    bc = df.DirichletBC(V, df.Constant(0), boundaries, 9)
    bc.apply(A, b)
    point = df.Point(readout_pos[0], readout_pos[1], readout_pos[2])
    delta = df.PointSource(V, point, 1.0)
    delta.apply(b)
    phi = df.Function(V)

    df.solve(A, phi.vector(), b, solver_params["linear_solver"], solver_params["preconditioner"])
    for idx, pos in enumerate(charges_pos):
        values[idx] = phi(pos)
    return values


def moi_value(charges_pos, moi, mesh_dict, elec_r, elec_pos, npts=100):
    """ Find the value at the electrode using the Method of Images (MoI) with electrode
    """

    xmid = charges_pos[:, 0]
    ymid = charges_pos[:, 1]
    zmid = charges_pos[:, 2]
    include_elec = elec_r != None
    ext_sim_dict = {'include_elec': include_elec,
                    'elec_x': np.array([0.]),
                    'elec_y': np.array([0.]),
                    'elec_z': elec_pos[2] + moi.h / 2,  # Different conventions from ViMEAPy
                    'n_avrg_points': npts,
                    'elec_radius': elec_r,
                    'use_line_source': False,
                    }
    mapping = moi.make_mapping_cython(ext_sim_dict, xmid=xmid, ymid=ymid, zmid=zmid + moi.h / 2)
    ground = 0
    moi_value_cython = mapping - ground
    return moi_value_cython

def elec_impact_simulations():
    
    solver_params = {'linear_solver': 'cg',
                     'preconditioner': 'ilu',
                     'LagrangeP': 2,
                     }

    df.parameters["krylov_solver"]["relative_tolerance"] = 1e-10

    out_folder = 'electrode_results'
    if not os.path.isdir(out_folder): os.mkdir(out_folder)
    elec_pos = np.array([0., 0., elec_z + epsilon], dtype='float')

    moi = ViMEAPy.MoI(**set_control)
    electrodes = ['E1', 'E2', 'E3']

    for elec_name in electrodes:
        print elec_name
        elec_r = mesh_dict['elec_r%s' % elec_name[1]]
        charge_z_pos = np.linspace(0.5, 10, 10) * elec_r + elec_z
        charges_pos = np.zeros((len(charge_z_pos), 3), dtype='float')
        magnitudes = np.ones(len(charges_pos))
        charges_pos[:, 2] = charge_z_pos
        np.save(join(out_folder, 'positions_%s.npy' % elec_name), charges_pos)

        elec_pts_to_average = 100
        moi_values = moi_value(charges_pos, moi, mesh_dict, elec_r, elec_pos, elec_pts_to_average)
        np.save(join(out_folder, 'moi_%s_%d.npy' % (elec_name, elec_pts_to_average)), moi_values[0, :])
        
        elec_pts_to_average = 1
        moi_values = moi_value(charges_pos, moi, mesh_dict, None, elec_pos, elec_pts_to_average)
        np.save(join(out_folder, 'moi_%s_%d.npy' % (elec_name, elec_pts_to_average)), moi_values[0, :])

        fem_values = electrode_fem_recip(solver_params, charges_pos, elec_pos, magnitudes, mesh_dict, elec_name)
        np.save(join(out_folder, 'fem_recip_%s.npy' % elec_name), fem_values)


def make_figure_5():

    folder = 'electrode_results'
    n_avrg_points = 100
    
    plt.close('all')
    fig = plt.figure(figsize=[4., 4.5])
    elec_names = ['E1', 'E2', 'E3']
    ax0 = fig.add_axes([0.6, 0.72, 0.25, 0.2], yscale='log', xlabel='Source height [e.r.]',
                      ylabel='Relative error')
    ax0.axis([0, 10, 1e-3, 1e0])

    elec_name_pos = [5e-1, 1.5e-1, 5e-2]
    
    for elec_idx, elec_name in enumerate(elec_names):
        charges_pos = np.load(join(folder, 'positions_%s.npy' % elec_name))[:, 2]
        clr = 'rbg'[elec_idx]
        elec_r = float(mesh_dict['elec_r%s' % elec_name[1]])

        charge_heights = charges_pos - elec_z
        moi_avrg = np.load(join(folder, 'moi_%s_%d.npy' % (elec_name, n_avrg_points)))
        moi_1 = np.load(join(folder, 'moi_%s_%d.npy' % (elec_name, 1)))
        fem = np.load(join(folder, 'fem_%s.npy' % elec_name))
        normalize_factor = np.max([fem])

        fem /= normalize_factor
        moi_avrg /= normalize_factor
        moi_1 /= normalize_factor

        rel_error_fem_elec_moi_no_elec = np.abs(fem - moi_1)/fem
        rel_error_elec = np.abs(fem - moi_avrg)/fem
        ax0.plot(charge_heights/elec_r, rel_error_elec, '-', lw=2, color=clr)
        ax0.plot(charge_heights/elec_r, rel_error_fem_elec_moi_no_elec, '--', lw=2, color=clr)
        ax0.text(10.1, elec_name_pos[elec_idx], '%d $\mu m$' % elec_r, color=clr, size=10)

    ax0.plot(1000, 1000, '-', color='k', lw=2, label='MoI disc electrode')
    ax0.plot(1000, 1000, '--', color='k', lw=2, label='MoI point electrode')
    simplify_axes(ax0)
    ax0.grid(True)
    ax0.legend(bbox_to_anchor=(1.2, 1.45), handlelength=3, frameon=False)

    positions = np.load(join(folder, 'lateral_grid_positions.npy'))

    elec_dists = 5
    elec_r = 10
    
    x = positions[:, 0].reshape(30, 30).T / elec_r
    z = (positions[:, 2].reshape(30, 30).T + slice_thickness/2.) / elec_r
    moi_1 = np.load(join(folder, 'moi_lateral_grid_1.npy'))[0, :]
    moi_avrg = np.load(join(folder, 'moi_lateral_grid_%s.npy' % n_avrg_points))[0, :]
    fem = np.load(join(folder, 'fem_lateral_recip_grid.npy'))
    
    normalize_const = np.max(fem)

    moi_1 = moi_1.reshape(30, 30).T / normalize_const
    moi_avrg = moi_avrg.reshape(30, 30).T / normalize_const
    fem = fem.reshape(30, 30).T / normalize_const

    error_1 = np.abs(fem - moi_1)
    #rel_error_1 = np.abs( (fem - moi_1)/fem)

    error_avrg = np.abs(fem - moi_avrg)
    #rel_error_avrg = np.abs((fem - moi_avrg)/fem)

    ax_kwargs = {'frameon': False,
                 'xlim': [0, elec_dists],
                 'ylim': [0, elec_dists],
                 'aspect': 'equal'
                 }
    
    ax_width = 0.35
    ax_height = 0.2
    ax1 = fig.add_axes([0.06, 0.73, ax_width, ax_height], title='FEM', xlabel='Lateral position [e.r.]',
                 ylabel='Source height [e.r.]', **ax_kwargs)
    ax2 = fig.add_axes([0.06, 0.38, ax_width, ax_height], title='MoI point electrode', **ax_kwargs)
    ax5 = fig.add_axes([0.06, 0.06, ax_width, ax_height], title='MoI disc electrode', **ax_kwargs)
    ax3 = fig.add_axes([0.55, 0.38, ax_width, ax_height], title='Error', **ax_kwargs)
    ax6 = fig.add_axes([0.55, 0.06, ax_width, ax_height], title='Error', **ax_kwargs)
    
    extent = [0, np.max(x)/elec_r, 0, np.max(z)/elec_r]

    contourf_kwargs = {'origin': 'lower',
                     'rasterized': True,
                     'cmap': plt.cm.gray_r,
                     'norm': LogNorm()
                     }
    
    contour_kwargs = {'origin': 'lower',
                     'extent': extent,
                     'colors': 'k',
                     }
    clevels_fields = [0.01, 0.1, 0.25, 0.5, 0.75, 1., 3.]
    clevels_errors = [1e-5, 1e-2, 1e-1, 1e0, 1e1]

    im1 = ax1.contourf(x, z, fem, levels=clevels_fields,  **contourf_kwargs)
    im2 = ax2.contourf(x, z, moi_1, levels=clevels_fields, **contourf_kwargs)
    im5 = ax5.contourf(x, z, moi_avrg, levels=clevels_fields,  **contourf_kwargs)
    im3 = ax3.contourf(x, z, error_1, levels=clevels_errors, **contourf_kwargs)
    im6 = ax6.contourf(x, z, error_avrg, levels=clevels_errors,  **contourf_kwargs)

    ax1.contour(x, z, fem, levels=clevels_fields, **contour_kwargs)
    ax2.contour(x, z, moi_1, levels=clevels_fields, **contour_kwargs)
    ax3.contour(x, z, error_1, levels=clevels_errors, **contour_kwargs)
    ax5.contour(x, z, moi_avrg, levels=clevels_fields, **contour_kwargs)
    ax6.contour(x, z, error_avrg, levels=clevels_errors, **contour_kwargs)
    

    ax_list = [ax1, ax0, ax2, ax3, ax5, ax6]
    
    mark_subplots(ax_list)
    [ax.plot([0, 1], [-.1, -.1], '-', lw=2, color='k', clip_on=False) for ax in ax_list]

    fields = [[ax1, im1], [ax2, im2], [ax5, im5]]
    for ax, im in fields:
        cbar = plt.colorbar(im, ax=ax, shrink=0.8)
        cbar.set_ticks(clevels_fields)
        cbar.set_ticklabels(['%1.2f' % tick for tick in clevels_fields])
        cbar.solids.set_rasterized(True)
        
    for ax, im in [[ax3, im3], [ax6, im6]]:
        cbar = plt.colorbar(im, ax=ax, shrink=0.8)
        cbar.solids.set_rasterized(True)

    plt.savefig('figure_5.png', dpi=150)

def simulate_lateral_grid():
    
    solver_params = {'linear_solver': 'cg',
                     'preconditioner': 'ilu',
                     'LagrangeP': 2,
                     }
    df.parameters["krylov_solver"]["relative_tolerance"] = 1e-10

    out_folder = join('electrode_results')
    epsilon = 1e-15
    elec_pos = [0., 0., elec_z + epsilon]

    elec_name = 'E2'
    elec_r = 10.
    
    charge_z_pos = np.linspace(1, 5 * elec_r, 30) + elec_z
    charge_x_pos = np.linspace(0, 5 * elec_r, 30)
    
    charges_pos = np.zeros((len(charge_x_pos)*len(charge_z_pos), 3))
    idx = 0
    idx_matrix = np.zeros((len(charge_z_pos), len(charge_x_pos)))
    for col, xpos in enumerate(charge_x_pos):
        for row, zpos in enumerate(charge_z_pos):
            charges_pos[idx, :] = [xpos, 0, zpos]
            idx_matrix[row, col] = idx
            idx += 1

    np.save(join(out_folder, 'lateral_grid_positions.npy'), charges_pos)
    magnitudes = np.ones(len(charges_pos))

    moi = ViMEAPy.MoI(set_control)
    n_avrg_points = 1
    moi_values = moi_value(charges_pos, moi, mesh_dict, None, elec_pos, n_avrg_points)
    np.save(join(out_folder, 'moi_lateral_grid_%d.npy' % n_avrg_points), moi_values)

    n_avrg_points = 100
    moi_values = moi_value(charges_pos, moi, mesh_dict, elec_r, elec_pos, n_avrg_points)
    np.save(join(out_folder, 'moi_lateral_grid_%d.npy' % n_avrg_points), moi_values)

    # lateral_values = electrode_fem(solver_params, charges_pos, elec_pos, magnitudes, mesh_dict, elec_name)
    # np.save(join(out_folder, 'fem_lateral_grid.npy'), lateral_values)
    lateral_values_recip = electrode_fem_recip(solver_params, charges_pos, elec_pos, magnitudes,
                                               mesh_dict, elec_name)
    np.save(join(out_folder, 'fem_lateral_recip_grid.npy'), lateral_values_recip)

if __name__ == '__main__':

    simulate_lateral_grid()
    elec_impact_simulations()
    make_figure_5()